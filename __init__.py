#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# To the extent possible under law, the author(s) have dedicated all
# copyright and related and neighboring rights to this software to the
# public domain worldwide. This software is distributed without any
# warranty.
# You should have received a copy of the CC0 Public Domain Dedication
# along with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

"""This package provides a single script solution doctool.

The script "run_gen_doc.py" generates a whole HTML documentation
for python code. By default, the docs will be located into folder
"public" where the folder structure of the package, for which the
docs are generated for, is reflected. Put this script into the
base folder of your python package and execute the script. That's
all.

For further information and documentation please refer to the
code reference or to the GitLab page which is linked below.

Author: Martin Damian Trochowski
Created on: 2020-03-28
Repository: https://gitlab.com/m.d.trochowski/run_gen_doc
"""
