# Description

This is a doctool for python (>= 3.6) which generates HTML documentation for
all python code in a whole python package. The output documentation is very
similar to other code reference HTML websites. The documentation includes
a seperate HTML file for each python module/file where the folder hierarchy of
the python package is reflected in the target folder. Furthermore, there is an
index.html in the base folder of the docs. Each HTML file includes the
docstring, functions, classes and global variables of the corresponding python
module/file.


# Where does this tool take the documentation from?

This tool takes so called docstrings directly from python code to support
documentation of code which is a standardized way to document python
code. See [PEP 257](https://www.python.org/dev/peps/pep-0257/) for more
information on docstrings and how to document the code properly. There
is also plenty of information on this topic on the web and video
platforms such as YouTube. You can also have a look on the file "test_module.py"
to get an example on how documentation in code can be done. You can
find the corresponding HTML doc
[here](https://m-d-trochowski.gitlab.io/run_gen_doc/test_module.html).

It is worth mentioning that this tool is also capable to capture
docstrings of global variables and class attributes according to
[PEP 258](https://www.python.org/dev/peps/pep-0258/). In order to be part of
the docs, the global variable or attribute assignment must be followed by a
string enclosed by """ which is considered the docstring.

The description of the package which is shown in the index.html file is taken
from the docstring of the "\_\_init\_\_.py" file in the base folder. If you
basically don't need an init file, you can still create one to be able to
leave a package description.

Note that there is a distinction between what is mandatory by means of the
existing design guidelines (PEP's) and different types of "docstyles"
(e.g. "google docstyle"). The docstyles differ in the way how docstrings
themselves are parsed. Unfortunately, there are different docstyles
around so this part is not standardized.

Therefore, this tool primarily aims to conform to all of the relevant PEP's.
Currently, basically all docstyles are supported as this tool simply takes the
docstrings as they are and outputs them directly to HTML files after some
basic cleanup. So there is no parsing of the docstrings themselves. However,
in the future it is planned to implement an intelligent pattern
recognition to be able to capture certain parts of docstring such as parameter
description and display them in a more convenient way in the HTML documents.

Note that by default and by standard the script skips over names which
begin with a single underscore _. Thus, this mechanism can be used to
exclude such parts from the documentation. Another way is to
use the name filters in the options. Also, the script skips over the
"main" part of each module; the part which is after  
`if name == '__main__':`  
Therefore, you can also put global variables there to exlucde them from the
docs.


# How to use?

This tool is very easy to use. You simply have to put the file
"run_gen_doc.py" into your base path of your python package/project
and execute it from anywhere. The script will automatically genrate a
documentation in the folder "public" (can be changed using options, though).
The folder will be created, too, if necessary.

This is a single script solution which was chosen in order to provide such
a simple handling.

If your operating system has setup python in the environment path, you can
"double click" this file in the file manager or execute it directly
on the command line or call this file in some python interpreter or
development environment.

If you have another python script which includes some more stuff like
linter and testing, you can also import this python file into the
other python script and call the "main()" function which is completely
equivalent to executing this script. You can modify the options
in the other script like this:
`import run_gen_doc`  
`run_gen_doc.HTML_BASE_FONT_SIZE = 14`  
Afterwards, you run the main function and the changed options will come into
effect:  
`run_gen_doc.main(path='./path_to_code/')`

# Benefits

An important benefit is that this tool, apart from other tools, parses the
source code, only, without executing the python modules themselves (it uses the
abstract syntax tree output from python itself). Unintended execution of python
modules might lead to unintended behaviour. The "solution" to this problem
in other doctools is to put all of the executable part of the module after the
line "`if __name__ == '__main__':`" which means that all scripts must
be changed accordingly which is not necessary when using this tool.

You can generate documentation without the need to actually have
all of the dependencies of the python code installed. This is very useful when
using continious integration to build the documentation on push/commit to
project repositories as some servers do not necessarily have every single
needed python module installed.

One of the rationales behind this project is to enable code quality
authorities in companies to encourage the teams to integrate code documentation
in a way which is as easy as possible to integrate by the teams; without the
need to do a lot of configuration. This is further facilitated by the fact
that this single script solution does not have any dependencies apart from
python 3.6 or higher and the fact that this script is public domain. So you
can even use it in proprietary projects without any need to worry or change
it as you please. Therefore, some code quality authorities may come up with
a customized version of this script (e.g. changing some of the settings and
the appearance).


# Customization

The generated docs can be configured using numerous options which are at
the very top of the script (directly after the public domain disclaimer
and the mandatory description docstring). Please refer to the [code reference
of this script](https://m-d-trochowski.gitlab.io/run_gen_doc/run_gen_doc.html)
to check out the options or go into the script to see all of the options. The
available options should be enough to allow for the vast majority of needed
customization.

The most notable options are `DOCSTRING_PARSER_ENABLED` together with
`DOCSTRING_PARSER_INTELLIGENT_NEWLINE`. The intelligent newline feature allows
to capture paragraphs in docstrings correctly instead of rendering them 1:1
"as they are in the code". This leads to much more satisfying documents in most
cases. However, as this feature is also "risky" because newlines may be set
incorrectly in very few cases this is disabled by default. 

The design of the HTML website can be changed using the option "CSS" which is
the CSS file which will be used in the docs. However, to do so you at least
need some basic understanding of CSS. There are numerous tutorials on this
topic on the web.

Note that it is not planned to enable customization via HTML templates as this
way is considered to be more complicated. Instead, some stuff regarding the
layout can be configured via the options which leads to faster results.


# Code Reference

[LINK](https://m-d-trochowski.gitlab.io/run_gen_doc/index.html)
* [run_gen_doc](https://m-d-trochowski.gitlab.io/run_gen_doc/run_gen_doc.html)
* [test_module](https://m-d-trochowski.gitlab.io/run_gen_doc/test_module.html)


# Automatic publishing of docs on GitLab

You can use the file ".gitlab-ci.yml" to enable the publishing of docs which
are located in the "public" folder. Simply add this file to your GitLab
repository and everything else runs automatically. This way, on each commit,
the current content of the "public" folder will be published. You can find the
link to your GitLab pages (doc website) under "Settings" -> "pages".

Note that I am not planning to provide similar support for GitHub as I do not
have a GitHub account and I do not plan to get one. However, if someone who
uses GitHub want's to contribute, I would appreciate it very much if someone
could provide corresponding configuration files for GitHub and possibly a short
manual how to get the docs published if it will not be done automatically via
the GitHub config files.


# Where can I download this module or install this as a module into my python environment (e.g. pip)?

Download the script "run_gen_doc.py" directly from this project on GitLab. This
way, you always get the latest version of the script. I will try to avoid
getting the master into an unstable state.

For the moment, it is not planned to provide a package via pip as this is a
single script solution where this should not be needed. It also doesn't allow
to change the options directly in the script.


# Further Project Documentation

You find some further project documentation in the folder "doc".

If you are a developer who is interested in how the code works, there is some
very useful [stuff in the doc folder](./doc/for_developers.pdf)
, too.

If you are interested in modifying the CSS or come up with your own CSS, there
is also [very useful stuff](./doc/for_designers.pdf)
in this folder. The HTML layout is shown, there.

* [Ideas](./doc/ideas.md)
* [Coding Guidelines](./doc/guidelines.md)
* [Implementation Notes](./doc/implementation_notes.md)
