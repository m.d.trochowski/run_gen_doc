Ideas for upcoming features
---------------------------

The following list is a loose list of ideas which may or may not be implemented
in the future:

* docstring parser pattern matcher
    * simple stuff
    * stuff which looks nice but does not hurt if the pattern is not recognized
    * e.g. headings
    * e.g. basic markdown stuff like bold or italic
        * This is actually not so straithforward as pure XML is currently used
          to generate the HTML files. This means that each HTML tag con only
          contain other HTML tags or pure text, strong hierarchical structure
          (strictly speaking, xml.etree allows a single tag after or before
          text but this is not used). So no tags can be "mixed into" a string
          which is pretty much necessary to tag some pieces of a string as
          "strong" or similar. Best solution would be to look out for some HTML
          module for python.
* show package dependencies in index
    * When using AST for doc generation we can actually only get "direct
      dependencies" because we look directly into the sources. Even if we know
      the direct dependencies we cannot get the AST of the directly imported
      modules because we cannot localize them properly or at least I do not
      know a function to retrieve the path for a given a module specifier
      instead of importing it. However, even if we know the paths to all
      modules it is not guaranteed that these modules are actually present on
      the current machine (because they don't need to for this tool).
      Nevertheless, I think the direct dependencies would be enough.
* command line options
* other output formats (like markdown or rst)
* Maybe provide some pip package anyways?
* pretty print XML
    * xml.etree doesn't allow this
    * minidom would support this but breaks some other stuff

For the moment I am satisfied with the results so I don't see a need to
implement much new functionality, especially given the current number of users
of this script. Bugfixes and adaptation of newer functions in future python
versions will be done, of course. I am using this for my own work regularly
so I will keep maintaining it. Maybe if this tool "booms" and there are lots of
feature requests I will implement some more stuff.
