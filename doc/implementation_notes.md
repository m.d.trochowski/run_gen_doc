Implementation Notes
====================

* As of Python 3.8, there is the very useful function “ast.get_source_segment”
* Thus, the functions “_ast_args_to_arg_docs”, “_extract_default” and
  “_extract_value” are mostly obsolete as of Python 3.8.
* However, these functions were written to provide support for Python 3.6, too.
* This will change when Python 3.8 is default on stable Debian release
  and this release is on most servers which use Debian (probably 2 years after
  that release; so quite some time to pass)
