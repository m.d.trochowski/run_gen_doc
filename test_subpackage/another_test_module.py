#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# To the extent possible under law, the author(s) have dedicated all
# copyright and related and neighboring rights to this software to the
# public domain worldwide. This software is distributed without any
# warranty.
# You should have received a copy of the CC0 Public Domain Dedication
# along with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

"""test class
    here we have some preceding white space

Author: Martin Damian Trochowski
Created on: 2020-02-22
"""



GLOBAL_VAR_1 = 1  # with some comment

GLOBAL_VAR_2 = (1, 2, 3)  # tuple with three elements
"""Global var 2"""

GLOBAL_VAR_3 = (GLOBAL_VAR_1 * 2) + 1
"""Global var 3"""

GLOBAL_VAR_4 = \
\
True  # with some line continuations

GLOBAL_VAR_5 = (1 +  # with line brake using brackets
                41)

GLOBAL_VAR_6 = {1, 2, 3,  # set with line break
                4, 5}

GLOBAL_VAR_7 = [1, 2, 3, 4,  # list with line break
                5, 6, 7]



from math import sqrt



class Test:
    """Test class."""
    
    A = None
    """This is attribute A.
    
    type:
        int
    description:
        Description A.
    """
    
    B = None
    
    def __init__(self, A, B):
        """Initializes the object.
        
        See attributes description in class description.
        """
        
        # attributes
        self.A = A
        
        self.B = B
        """This is wonderful attribute B!
        
        type:
            int
        description:
            Description B.
        """
    
    def fata(self, var):
        """fata method
        
        Returns:
            fata
        
        Args:
            var: 
                type:
                    int
                description:
                    Description.
        """
        return (var / self.B) * self.A
    
    def morgana(self, var):
        """morgana method
        
        Returns:
            morgana
        
        Args:
            var:
                type:
                    int
                description:
                    Description.
        """
        return (var * self.A) + self.B
    
    def _helper(self):
        return self.A + self.B



class Test2(Test):
    """Derivation of class Test."""
    def __init__(self):
        """initializer"""
        super().__init__(self, 1, 1)
        self._C = 1
        self._D = 2
        
        self.E = 3
        """This is inside __init__."""
    
    @property
    def C(self):
        """This is attribute C!"""
        return self._C
    
    @C.setter
    def C(self, C):
        """setter of C"""
        self._C = C
    
    @property
    def D(self):
        """This is amazing attribute D!"""
        return self._D
    
    @D.setter
    def D(self, D):
        self._D = D



def test_function(a, b, c=None, d=1, e=Test(1,2), f=sqrt(2)):
    """This is a test function."""
    intermediate = (a + b) * d
    
    if c:
        intermediate += c
    
    return intermediate


def test2(*args, a, b=1, c, **kwargs):
    accumulator = 0
    
    for arg in args:
        accumulator += arg
    
    for key, value in kwargs.items():
        accumulator += value


GLOBAL_VAR_AT_END = '12345'
