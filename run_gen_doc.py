#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# To the extent possible under law, the author(s) have dedicated all
# copyright and related and neighboring rights to this software to the
# public domain worldwide. This software is distributed without any
# warranty.
# You should have received a copy of the CC0 Public Domain Dedication
# along with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

"""Script which generates documentation for the whole package.

Author: Martin Damian Trochowski
Created on: 2020-03-01
Source: https://gitlab.com/m-d-trochowski/run_gen_doc
"""



###########
# OPTIONS #
###########

CLASS_VARIABLE_TEXT = 'class variable'
"""This string will be shown as a hint in an attribute when it is a class
variable.
"""

DOC_PATH = './public'
"""Absolute or relative path (str) to the folder where the HTML documentation
shall be stored. If this folder does not exist, it will be created, first."""

DOCSTRING_PARSER_ENABLED = False
"""If this bool is "True", the script will do further parsing of the
docstrings. If you want the docstrings to be displayed "as is", you can set
this to "False". This way, you will definitely get a proper display of your
docstrings as there is no room for misinterpretation which can happen at times
depending on the style of the docstrings.
"""

DOCSTRING_PARSER_INTELLIGENT_NEWLINE = True
"""If this bool is "True", the docstring parser will collapse multiple lines
in a docstring into a single paragraph if the docstring in this region seems
to look like the text belongs together in a single paragraph. This means that
the parser generates newlines whenever it seems appropriate. The benefit of
this is that a block of text in a docstring will also be displayed as such in
the rendered output without premature newlines where there actually is enough
space on the right hand side of the display. Generally, multiple lines in a
docstring can only be merged together into a paragraph if all of the lines are
at the same indentation level. Furthermore, if there is at least a single blank
line between two lines of text, they will never be considered to belong
together. Last but not least, if a line ends more early than usually (some
statistics are invoked, here) there will also be a newline.
"""

HEADING_ATTRIBUTES = 'Attributes'
"""String which determines the heading of the class attributes section."""

HEADING_CLASSES = 'Classes'
"""String which determines the heading of the classes section."""

HEADING_CLASS_DESCRIPTION = 'Description'
"""This string will only be shown if the class inherits from some
superclasses. This string determines the text which is above the description
of the class which is the formatted or unformatted docstring of the class.
Therefore, if there is no class docstring, there also won't be this heading.
"""

HEADING_CLASS_INHERITANCE_LIST = 'Inherits from:'
"""This string is the shown heading text above the inheritance list of a class
if the class inherits from some superclasses. If it does not inherit, this
heading won't be shown at all.
"""

HEADING_FUNCTIONS = 'Functions'
"""String which determines the heading of the functions section."""

HEADING_GLOBAL_VARS = 'Global Variables'
"""String which determines the heading of the global variables section."""

HEADING_METHODS = 'Methods'
"""String which determines the heading of the methods section of a class."""

HTML_BASE_FONT_SIZE = 1.8
"""Determines the size of all rendered HTML text in points. Note that all HTML
elements have a font size which is proportional to this font size by default.
"""

#HTML_PRETTY_XML = True
#"""If set to "True", the HTML documents get proper format so that it can
#be conveniently red by humans.
#"""

#HTML_PRETTY_XML_INDENT = '  '
#"""If the option "HTML_PRETTY_XML" is set to "True", this option determines
#the characters which are used for a single indentation level.
#"""

INDEX_LINK_TEXT = 'back to index'
"""This is the text of the link back to the index which is shown in each module
HTML file if the option "INDEX_SHOW_LINK" is set to "True".
If this is "None", the link will be displayed "as is".
"""

INDEX_SHOW_LINK = True
"""If "True", each module HTML file contains a link back to the index."""

INDEX_SHORT_DESCRIPTION = True
"""If "True", the index provides short descriptions for the shown modules if
available. Note that the short description of each module (each docstring,
actually) is the very first non empty line of the docstring according to PEP
257. If there already is a lot of source code which does not follow this
convention, you can set this to "False".
"""

IGNORE_ATTRIBUTES = False
"""If set to "True" the script will ignore all attributes in classes (class
variables, instance variables, properties). This will not affect attributes in
the class docstring. This can be handy if the main place of attribute
documentation in a project is in class docstrings rather than anywhere else.
This could lead to a problem as there would be an additional "attributes"
section in the docs even if they are already described in the class docstring.
If you want the script to collect all attributes and their docstrings, set this
to "False". 
"""

IGNORE_FILE_NAMES = set()
"""Set of file name strings without file extension to be ignored. All files
mentioned, here, will not be part of the generated docs. Thus, this really
affects python files, only.
"""

IGNORE_FOLDER_NAMES = {'.git'}
"""Set of folder names to be ignored. It is enough that any part of the path
after the base folder path contains any of the specified folder names here.
This is useful to exclude some larger subfolder hierarchies from the search
such as a larger datasets or if you have some python subpackages which are from
an external source which should not be included into the docs.
"""

IGNORE_GLOBAL_VARS = False
"""If set to "True" no global variables in no module will be part of the docs.
"""

IGNORE_NAMES = set()
"""All the names in this set will be ignored and will not be part of the
documentation. This applies to any symbol name in python code.
"""

IGNORE_NAME_PREFIXES = {'_'}
"""Tuple of strings which represent the prefix of each python symbol name in
the code which shall be ignored; thus being excluded from the generated
documentation.
"""

IGNORE_PROPERTIES = False
"""If this is "False", the script will see properties in classes which also
come with a docstring as attributes which will be part of the "attributes"
section of the class where this property was found. However, if the main
place to describe attributes in a project is in the class docstring rather
than the property docstring (property may contain short description, only; e.g.
to satisfy linters), this might be annoying as you would have two places in the
docs where the same attribute is mentioned. A similar problem may arise if the
main place to document attributes is in the __init__ method as the attributes
defined in the __init__ method have less precedence. Both problems can be
adressed by setting this option to "True".
"""

IGNORE_VALUES = set()
"""This set of strings determines the python symbol names where the value
should not be included into the docs. This can be useful to prevent variables
with overly long values to be included into the docs; thus, getting the docs
more clean.
"""

IGNORE_WITHOUT_DOCSTRING = False
"""If this is set to "True", all symbols in the source code are ignored which
do not come with a docstring. This may be convenient if the source code has a
lot of symbols without a docstring intentionally which also do not have the _
prefix. However, the more clean solution is either to exclude such symbols from
the docs via the _ prefix or to simply come up with docstrings in all cases.
Setting this to "False" temporarily might also be useful to find all symbols
which actually do not come with docstrings as this becomes quite apparent in
the generated docs.
"""

ORDER = ('global_vars', 'functions', 'classes')
"""Determines order in the output documentation.
This must include the entries 'global_vars', 'functions' and 'classes' in any
order in a tuple or list.
"""

LINK_PROJECT = None
"""If you want to have a link to the project website on the doc index, you can
set the link, here. Otherwise, set this to "None".
"""

LINK_PROJECT_TEXT = 'project link'
"""If option "LINK_PROJECT" is not "None", this will be the text of the link
to the project website. If this is set to "None", the link will appear as is.
"""

LINK_REPO = None
"""If you want to have a link to the repository website on the doc index, you
can set the link, here. Otherwise, set this to "None".
"""

LINK_REPO_TEXT = 'repository link'
"""If option "LINK_REPO" is not "None", this will be the text of the link to
the repository website. If this is set to "None", the link will appear as is.
"""

PACKAGE_NAME = None
"""If this is "None", the package name will be the base folder name which is
the folder name of the folder for which the docs will be generated for (by
default the folder in which this script resides). If this is not desired, you
can specify another name, here.
"""

REPLACE_INIT_METHOD_NAME = True
"""If "True", the name of the "__init__" method in all classes will be
replaced with the class name. This is convenient most of the time as this is
the form how the user calls the constructor.
"""

SORT_ATTRIBUTES = True
"""If "True", the detected attributes in a class will be sorted.
If "False", the order will be as detected in the source code where attribute in
the class body have preference; attributes in __init__ method come second.
"""

SORT_CLASSES = True
"""If "True", the classes will be sorted by name. If "False", the order will be
as in the soruce code.
"""

SORT_INHERITANCE_LIST = True
"""If "True", the shown inheritance list in the docs of a class will be sorted
by name (lexicographical). If "False", the order will be as in the source code.
"""

SORT_FUNCTIONS = True
"""If "True", the functions will be sorted by name. If "False", the order will
be as in the source code.
"""

SORT_GLOBAL_VARS = True
"""If "True", the global variables will be sorted by name. If "False", the
order will be as in the source code.
"""

SORT_METHODS = True
"""If "True", the methods in each class will be sorted by name. If "False", the
order will bne as in the source code.
"""

SPACES_PER_TAB = 4
"""If tabs are used in any docstring, this is the multiplier which is used to
determine the indentation level of each single tab. Note that the "indentation
level" is kind of measured in units of white spaces (although these are not
rendered as white spaces; actual indentation is determined in CSS). This is
needed to handle tabs and white spaces for indentation in a similar manner.
Generally, setting this to a lower integer leads to less indentation in case
of tabs. It is recommended to set this to the value of displayed white spaces
per tab which was set in the editor in case this is consistent in a project.

Range of valid values: 1 - 8
"""


CSS = \
""":root {
font-family: "Courier New", monospace;
font-size: 16px;  /* fallback */
font-size: ###HTML_BASE_FONT_SIZE###vw;
}

@media (min-width: 4000px) {
    /* Ultra wide screens need some adaptation as the font size would simply 
       be to big. In this case, we put half of the specified size in here.
       Unfortunately, I cannot test this as I do not have such a display. */
    :root {
    font-size: 16px;  /* fallback */
    font-size: ###HALF_HTML_BASE_FONT_SIZE###vw;
    }
}

body {
padding-left: 3vw;
padding-right: 3vw;
}

h1 {
font-family: Arial;
font-size: 250%;
text-align: center;
}

h2 {
font-size: 150%;
margin: 0em;
margin-bottom: 1em;
margin-top: 0.25em;
padding: 0em;
text-align: center;
}

h2.index {
margin-top: 2em;
}

h3 {
font-size: 125%;
margin: 0em;
margin-bottom: 0.5em;
padding: 0em;
text-align: center;
}

h4 {
font-size: 110%;
margin: 0em;
margin-top: 2em;
margin-bottom: 0.5em;
text-align: left;
}

p {
text-align: left;
}

a:link {
color: #8bab69;
}

a:visited {
color: #558f18;
}

a:hover {
color: #558f18;
font-weight: bold;
}

a.index_link {
display: inline-block;
margin-bottom: 1.1em;
}

a.project_link {
display: inline-block;
margin-bottom: 1.1em;
}

a.repo_link {
display: inline-block;
margin-bottom: 1.1em;
}

div.module_docstring > p {
margin-top: 0em;
margin-bottom: 0em;
margin-right: 0em;
padding: 0em;
}

div.module_docstring > p.line {
white-space: pre;
}

div.classes, div.functions, div.global_vars {
margin: 0.2em;
margin-top: 3em;
padding: 0.5em;
}

div.classes {
background-color: #ececec;
border-color: #a4a4a4;
border-radius: 0.6em;
border-style: dashed;
border-width: 0.15em;
}

div.class {
background-color: #daffb3;
border-color: #558f18;
border-style: solid;
border-radius: 0.4em;
border-width: 0.13em;
margin: 0em;
margin-left: 0.25em;
margin-right: 0.25em;
margin-bottom: 2em;
padding: 1em;div.class {
background-color: #daffb3;
border-color: #558f18;
border-style: solid;
border-radius: 0.4em;
border-width: 0.13em;
margin: 0em;
margin-left: 0.25em;
margin-right: 0.25em;
margin-bottom: 2em;
padding: 1em;
}
}

div.class_docstring > p {
margin-top: 0em;
margin-bottom: 0em;
margin-right: 0em;
padding: 0em;
}

div.class_docstring > p.line {
white-space: pre-wrap;
}

div.attributes {
}

div.attribute {
padding: 0em;
margin: 0em;
margin-bottom: 1.6em;
}

div.attribute > p {
font-family: monospace;
font-size: 95%;
padding: 0em;
margin-bottom: 0.3em;
}

div.attribute > p.class_variable_hint {
font-size: 75%;
font-style: italic;
line-height: 100%;
margin: 0em;
margin-bottom: 0.7em;
padding-left: 3.25em;
white-space: pre-wrap;
}

div.attribute_docstring {
font-size: 85%;
line-height: 100%;
margin: 0em;
padding-left: 3em;
white-space: pre-wrap;
}

div.attribute_docstring > p {
margin-top: 0em;
margin-bottom: 0em;
margin-right: 0em;
padding: 0em;
}

div.attribute_docstring > p.line {
white-space: pre;
}

div.functions {
background-color: #ececec;
border-color: #a4a4a4;
border-radius: 0.6em;
border-style: dashed;
border-width: 0.15em;
}

div.function {
background-color: #daffb3;
border-color: #558f18;
border-style: solid;
border-radius: 0.4em;
border-width: 0.13em;
margin: 0em;
margin-left: 0.25em;
margin-right: 0.25em;
margin-bottom: 2em;
padding: 1em;
}

div.function > p {
font-family: monospace;
font-weight: bold;
margin: 0em;
margin-bottom: 0.7em;
padding: 0em;
}

div.function_docstring {
padding-left: 4ch;
font-size: 95%;
}

div.function_docstring > p {
margin-top: 0em;
margin-bottom: 0em;
margin-right: 0em;
padding: 0em;
}

div.function_docstring > p.line {
white-space: pre;
}

div.global_vars {
background-color: #ececec;
border-color: #a4a4a4;
border-radius: 0.6em;
border-style: dashed;
border-width: 0.15em;
}

div.global_var {
padding: 0em;
margin: 0em;
margin-bottom: 1.7em;
}

div.global_var > p {
font-family: monospace;
font-weight: bold;
padding: 0em;
margin-bottom: 0.5em;
}

div.global_var_docstring {
padding-left: 4ch;
font-size: 95%;
line-height: 100%;
}

div.global_var_docstring > p {
margin-top: 0em;
margin-bottom: 0em;
margin-right: 0em;
padding: 0em;
}

div.global_var_docstring > p.line {
white-space: pre;
}

div.method {
background-color: #c9c9c9;
border-color: #383838;
border-style: dotted;
border-radius: 0.4em;
border-width: 0.09em;
margin: 0em;
margin-left: 0.35em;
margin-right: 0.35em;
margin-bottom: 1.1em;
padding: 0.7em;
}

div.method > p {
font-family: monospace;
font-weight: bold;
margin: 0em;
margin-bottom: 0.72em;
padding: 0em;
}

div.method_docstring {
padding-left: 4ch;
font-size: 95%;
}

div.method_docstring > p {
margin-top: 0em;
margin-bottom: 0em;
margin-right: 0em;
padding: 0em;
}

div.method_docstring > p.line {
white-space: pre;
}

div.index_subpackage {
margin: 0em;
margin-bottom: 3.7em;
margin-top: 2em;
padding: 0em;
}

p.index_subpackage_name {
font-weight: bold;
font-size: 105%;
}

div.index_module {
margin: 0em;
margin-bottom: 1.2em;
padding: 0em;
}

a.index_module_name {
font-size: 110%;
}

p.index_module_description {
margin: 0em;
margin-left: 3ch;
margin-top: 0.3em;
padding: 0em;
}

p.indent0 {
margin-left: 0ch;
}

p.indent1 {
margin-left: 1ch;
}

p.indent2 {
margin-left: 2ch;
}

p.indent3 {
margin-left: 3ch;
}

p.indent4 {
margin-left: 4ch;
}

p.indent5 {
margin-left: 5ch;
}

p.indent6 {
margin-left: 6ch;
}

p.indent7 {
margin-left: 7ch;
}

p.indent8 {
margin-left: 8ch;
}

p.indent9 {
margin-left: 9ch;
}

p.indent10 {
margin-left: 10ch;
}

p.indent11 {
margin-left: 11ch;
}

p.indent12 {
margin-left: 12ch;
}

p.indent13 {
margin-left: 13ch;
}

p.indent14 {
margin-left: 14ch;
}

p.indent15 {
margin-left: 15ch;
}

p.indent16 {
margin-left: 16ch;
}

p.indent17 {
margin-left: 17ch;
}

p.indent18 {
margin-left: 18ch;
}

p.indent19 {
margin-left: 19ch;
}

p.indent20 {
margin-left: 20ch;
}

p.indent21 {
margin-left: 21ch;
}

p.indent22 {
margin-left: 22ch;
}

p.indent23 {
margin-left: 23ch;
}

p.indent24 {
margin-left: 24ch;
}

p.indent25 {
margin-left: 25ch;
}

p.indent26 {
margin-left: 26ch;
}

p.indent27 {
margin-left: 27ch;
}

p.indent28 {
margin-left: 28ch;
}

p.indent29 {
margin-left: 29ch;
}

p.indent30 {
margin-left: 30ch;
}

p.indent31 {
margin-left: 31ch;
}

p.indent32 {
margin-left: 32ch;
}
"""
"""CSS template
Variables to be replaced shall begin and end with a triple hashtag, e.g.
###some placeholder###
After replacement of the placeholders, this will be the exact content of the
CSS file.
"""



###########
# IMPORTS #
###########

import ast
from collections import deque
from functools import total_ordering
from inspect import cleandoc
from os import chdir
from os import getcwd
from os import listdir
from os import makedirs
from os.path import abspath
from os.path import dirname
from os.path import isdir
from os.path import isfile
from os.path import join
from os.path import normpath
from os.path import realpath
from os.path import relpath
from os.path import split
from os.path import splitext
from sys import version_info
from traceback import format_exc as format_exception
from xml.etree import ElementTree as XmlTree



#######################################
# DOCSTRING REPRESANTATION AND PARSER #
#######################################

class _Docstring:
    """Representation of a parsed docstring.
    
    Each docstring will be converted by the docstring parser to an object of
    this class if docstring parser is enabled.
    """
    
    def __init__(self):
        self.paragraphs = list()
        """Paragraphs of text."""
        
        self.indentation_levels = list()
        """For each paragraph in attribute "paragraphs", there is a
        corresponding entry in this list at the same index which stores
        the indentation level of that paragraph. Note that the definition of
        "indentation" is different, here, from the common sense as in case of
        white spaces for indentation, each single white space is considered an
        "indentation level". This is to account for some specific corner
        cases where the actual indentation may not match the general number
        of white spaces for indentation (e.g. aligning on ":").
        """
        
        #self.format = 'raw'
        #"""Can be "raw" or "parsed". In "raw" format, the indentation levels
        #do not matter and each line of the docstring is parsed "as is".
        #In "raw" format, each "paragraph" is rather a line of text.
        #"""



def _parse_docstring(docstring):
    """Parses the given docstring which is provided as a string.
    
    Returns:
        type:
            _Docstring or NoneType
        description:
            Is "None" if the input is "None", too.
        note:
            Please refer to the docs of the class to get more information on
            the extracted data.
    
    Args:
        docstring:
            type:
                str or NoneType
            description:
                The docstring to be parsed (if any at all).
    
    Raises:
        type:
            UserWarning
        description:
            If anything about the docstring is melformed in such a way that
            the parsing is not or partially not possible, a UserWarning
            will be raised containing the corresponding information.
            Please catch all of these warnings in the caller routine!
    """
    if docstring is None:
        return None
    
    
    lines = docstring.splitlines()
    
    
    # Determine the indentation level for each line in the docstring.
    # Note that "indentation level" here is defined as the number of
    # preceeding white spaces.
    lines_spaces = list()  # white spaces and tabs of each line
    lines_text   = list()  # text after indentation part of each line
    lines_blank  = list()  # Which lines are blank?
    
    for line_index, line in enumerate(lines):
        spaces      = list()
        split_index = None
        is_blank    = False
        
        for char_index, character in enumerate(line):
            if character in {' ', '\t'}:
                spaces.append(character)
            else:
                split_index = char_index
                break
        
        if split_index is not None:
            text = line[split_index :]
            lines_blank.append(False)
        else:
            text = ''
            lines_blank.append(True)
        
        spaces = ''.join(spaces)  # to string
        lines_spaces.append(spaces)
        lines_text.append(text)
        
        if (' ' in spaces) and ('\t' in spaces) and not is_blank:
            # If there is mixed indentation, we cannot come up properly
            # with indentation levels which is a key concept in this
            # parser, both, for later display of the paragraph text and
            # for the "intelligent newline" feature. For this reason and
            # to encourage the user to come up with proper docstrings,
            # end the parsing, here.
            raise UserWarning(f'Docstring contains mixed indentation (white '
                              f'spaces and tabs) at line {line_index}.')
    
    
    if not DOCSTRING_PARSER_INTELLIGENT_NEWLINE:
        raise ValueError('Docstring parser is enabled but option '
                         '"DOCSTRING_PARSER_INTELLIGENT_NEWLINE" is "False".'
                         'Currently, the only feature of the docstring '
                         'parser is the intelligent newline feature.')
    
    
    # intelligent newline feature
    paragraphs   = list()
    indentations = list()
    
    if DOCSTRING_PARSER_INTELLIGENT_NEWLINE:
        # When someone writes a docstring, he is usually writing against some
        # imaginary line (e.g. standard 79 columns limit). It is rarely the
        # case that this line is reached in docstrings but still the best way
        # to find this imaginary line is to take the maximum line length. This
        # definitely filters out all of the rather short lines such as
        # headings. This is done here on a per docstring basis. A problem with
        # this algorithm can occur when there is a  single line which is much
        # longer than the actual limit. This is considered to be a fault of the
        # user so no actions are taken. Another problem may occur when the
        # imaginary line is actually not reached a single time which actually
        # means that there should always be a newline. Here, it could happen
        # that newlines are skipped if two following lines are on the same
        # indentation level, near the limit and accidentially more or less
        # equal length. However, other doctools would combine these together
        # into a single paragraph, too, so that this solution is still better
        # overall.
        # Alternative approach:
        # Not only consider each docstring on it's own, but rather look at all
        # docstrings in a module or over all modules and take the maximum over
        # all of the lines after some outlier removal. However, as each
        # docstring can be specified at different absolute indentation levels
        # in the source code, the absolute length of each line must be
        # considered which is normally not the case as the docstrings are
        # "cleaned" as soon as possible. It would be necessary to store all
        # docstrings in their raw form, first. Furthermore, the cleaning and
        # parsing of the docstrings would be needed to be done in an extra
        # step after generation of the abstract doc tree. All of this could be
        # done but definitely needs considerably more effort. The question is
        # whether using a "global limit" rather than the currently implemented
        # "local limit" is actually better. I have the feeling that the local
        # limit solution is more adaptive and leads to more satisfying results
        # on average as it seems that not everybody or at least not always
        # writes against a globally set imaginary line in the source code.
        # Sometimes, the point where the programmer actually breaks can differ
        # from docstring to docstring. The local limit should work just fine
        # as long as the docstrings are long enough. If we have to deal with
        # rather short docstrings where no of the lines really reaches the
        # programmers "imaginary line", we cannot determine a reliable local
        # limit of course. In this (rather uncommon case) it might happen that
        # intented newlines are not recognized as such. Here is an example
        # docstring:
        # "
        # Some cool docstring text.
        #
        # Short header in docstring:
        # Text related to header.
        # "
        # The heading would be merged with the following line of text. To deal
        # with this special case one has to realize that the programmer wants a
        # newline if all of the lines of text are unreasonably short.
        # Therefore, a minimum limit of 35 should handle most of similar
        # examples correctly.
        
        
        # Search for non blank lines, first.
        non_blank_lines = list()
        
        for line_index in range(len(lines)):
            if lines_blank[line_index] == False:
                line = lines[line_index]
                non_blank_lines.append(line)
        
        # determine number of "displayed characters" per line
        # (Tabs are displayed as multiple white spaces in editor.)
        lines_char_count = list()
        
        for line in non_blank_lines:
            tabs = line.count('\t')
            
            # The minus one in the following formula comes from the fact that
            # the tab characters are already included in "len(line)" and we
            # only want to add the additional white spaces which are added due
            # to the tab.
            chars = len(line) + tabs * (SPACES_PER_TAB - 1)
            lines_char_count.append(chars)
        
        limit = max(lines_char_count)
        
        if limit < 35:
            # see big comment above
            limit = 35
        
        
        # Now, decompose the docstring into a set of paragraphs.
        current_paragraph     = list()  # Lines will be merged, later.
        paragraph_indentation = None    # Will be set and reset during loop.
        
        for line_index in range(len(lines)):
            is_blank    = lines_blank[line_index]
            line        = lines[line_index]
            spaces      = lines_spaces[line_index]
            text        = lines_text[line_index]
            indentation = len(spaces)
            
            if '\t' in spaces:
                # Note that an earlier check forces tabs or white space, only.
                indentation *= SPACES_PER_TAB
            
            if paragraph_indentation is None:
                paragraph_indentation = indentation
            
            
            if is_blank:
                # A blank line always closes the paragraph that is preceeding
                # the blank line.
                if len(current_paragraph) > 0:
                    current_paragraph = ' '.join(current_paragraph)
                    paragraphs.append(current_paragraph)
                    indentations.append(paragraph_indentation)
                
                # Blank lines shall be displayed as such. Therefore,
                # each blank line is also a paragraph.
                paragraphs.append('')
                indentations.append(indentation)
                current_paragraph     = list()
                paragraph_indentation = None
            
            elif len(current_paragraph) == 0:
                # First line in paragraph (for non-blank lines).
                current_paragraph.append(text)
            
            elif indentation != paragraph_indentation:
                # If the current line of text is on a different level of
                # indentation, we consider the current line to be part of a
                # new paragraph. Therefore, we have to finalize the "current
                # paragraph".
                current_paragraph = ' '.join(current_paragraph)  # to string
                paragraphs.append(current_paragraph)
                indentations.append(paragraph_indentation)
                current_paragraph     = [text]
                paragraph_indentation = indentation
            
            else:
                # Now, see whether the first word in this paragraph would
                # actually fit into the end of the previous line; including
                # some margin. The margin is because when someone writes the
                # docstring, he may consider whether the word is fitting "more
                # or less". Given some word, if the writer feels that it "could
                # be tight" to fit the word, he may break prematurely. The
                # margin is accounted for in form of the "effective length"
                # which is somewhat larger than the actual length of the word.
                # The factor is determined empiricaly using some example
                # docstrings. If the word would fit in the previous line, we
                # can assume a newline / new paragraph. Note that at this stage
                # there is always a previous line so that an index check is not
                # necessary.
                words            = text.split()
                first_word       = words[0]
                length           = len(first_word)
                effective_length = int(round(1.5 * length))
                previous_line    = lines[line_index - 1]
                
                if effective_length < 4:
                    # Looking at different docstrings it seems as though
                    # people sometimes break lines earlier than necessary when
                    # using very short words such as "a" or "be". I suspect
                    # this is so because a block of text appears as such
                    # regardles of whether a very short word is on one line or
                    # the next; because it is so short it does not distort the
                    # appearance of the text block much. We deal with this by
                    # using a minimum effective length which essentially means
                    # that we are more willing to continue the paragraph when
                    # the next line begins with very short word.
                    effective_length = 4
                
                if line_index == 1:  # if second line
                    # We assume that the first line in each docstring begins
                    # with a triple double quote -> """
                    # Even though this is a guess this is the case in most
                    # docstrings. Therefore, we should increase the actual
                    # length of the previous line by this amount because we
                    # might include an unnecessary newline, here, otherwise. In
                    # case that the first non blank line of the docstring does
                    # not begin with """ the consequence is that the effective
                    # line limit is increased by 3 which means that the second
                    # line must be very obviously a newline so that a newline
                    # occurs which is not bad actually.
                    previous_line_len = len(previous_line) + 3
                else:
                    previous_line_len = len(previous_line)
                
                if (previous_line_len + 1 + effective_length) > limit:
                    current_paragraph.append(text)
                else:
                    # Note the plus one: Don't forget about a white space.
                    # -> newline
                    current_paragraph = ' '.join(current_paragraph)
                    paragraphs.append(current_paragraph)
                    indentations.append(paragraph_indentation)
                    current_paragraph     = [text]
                    paragraph_indentation = indentation
            
            
            # extra handling of the last line needed
            # -> end current paragraph
            if line_index == (len(lines) - 1):
                current_paragraph = ' '.join(current_paragraph)
                paragraphs.append(current_paragraph)
                indentations.append(paragraph_indentation)
        
        docstring_object                    = _Docstring()
        docstring_object.paragraphs         = paragraphs
        docstring_object.indentation_levels = indentations
        
        return docstring_object



#####################
# ABSTRACT DOC TREE #
#####################

# Following classes model python modules as a hierarchy of classes:
# 
#                 _ModuleDoc  <- top level object
#      ________________|_______________
#     |                |               |
# _ClassDoc      _FunctionDoc   _GlobalVarDoc
#     |                |
# _FunctionDoc     _ArgDoc
#     |
#  _ArgDoc
#
# This allows to capture all of the needed data of each python module.
# This is useful in order to get rid of the not needed parts of the
# abstract syntax tree. It is much more convenient to handle this
# rather then the abstract syntax tree as the "ast" module is not
# documented properly and it is way more than needed for a doc.
# Note that this intermediate state also allows to go for different
# target output formats more easily.
# Note that each class is mostly a container for data, only. This
# is due to the fact that there is nothing like a "struct" in python
# besides dictionaries. However, it seems to be more cumbersome to
# handle nested dictionaries, here, instead of being able to access
# class attributes.
# Note that all of the modules shown in the graph above can contain
# a _Docstring object, too.

@total_ordering
class _DocBaseClass:
    name = None
    
    def __init__(self):
        raise NotImplementedError
    
    def __eq__(self, other):
        if type(other) != type(self):
            return False
        if (type(self.name) == type(None)) \
                and (type(other.name) == type(None)):
            return True
        if type(self.name) == type(None):
            return False
        if type(other.name) == type(None):
            return False
        return self.name == other.name
    
    def __gt__(self, other):
        if type(other) != type(self):
            raise TypeError(f'Cannot compare _ClassDoc to {type(other)}.')
        if (type(self.name) == type(None)) \
                and (type(other.name) == type(None)):
            return False
        if type(self.name) == type(None):
            return False
        if type(other.name) == type(None):
            return True
        return self.name > other.name



class _AttributeDoc(_DocBaseClass):
    """Represents a class attribute."""
    def __init__(self):
        self.docstring = None
        self.default   = None
        
        # Class variables are such which are defined directly in the
        # class body. In the docs, they will also be part of the
        # "attributes" section as from the perspective of the user
        # it is some value of the object or class which can be accessed
        # as an attribute. However, in case of class variables, it is
        # at least good to know whether this is a class variable or not.
        self.class_variable = False
    
    def __str__(self):
        string = self.name
        
        # OLD
        #if self.value:
        #    string += f' = {self.value}'
        
        return string



class _ArgDoc(_DocBaseClass):
    """Represents doc of an argument in a python function or method."""
    def __init__(self):
        self.default    = None
        self.is_varargs = False  # variable argument list, e.g. *args
        self.is_kwargs  = False  # keywords arguments dict, e.g. **kwargs
        
        # Note that the so called "keyword only arguments" do not get an
        # own representation in this class as these can be treated as
        # regular arguments with a name and with or without some default
        # value so a name and a deafult is everything which is needed
        # to catch these when it comes to the documentation.
    
    def __str__(self):
        if self.name == None:
            return ''
        
        string = self.name
        
        if self.default:
            string += ' = ' + self.default
        
        if self.is_varargs:
            string = '*' + string
        
        if self.is_kwargs:
            string = '**' + string
        
        return string



class _ClassDoc(_DocBaseClass):
    """Represents doc of a top level class in a python module."""
    def __init__(self):
        self.docstring    = None
        self.methods      = list()
        self.superclasses = list()  # a.k.a. inheritance list
        self.attributes   = list()
    
    
    def get_attribute(self, name):
        """Get attribute by name.
        
        Returns:
            type:
                tuple or None
            description:
                "None" if there was no attribute found. Otherwise:
                [0]: _AttributeDoc object of interest
                [1]: index into ".attributes" list
        """
        for index in range(len(self.attributes)):
            attribute = self.attributes[index]
            
            if attribute.name == name:
                return (attribute, index)
        
        return None
    
    
    def put_init_first(self):
        if REPLACE_INIT_METHOD_NAME:
            search_term = self.name
        else:
            search_term = '__init__'
        
        init_index = -1
        
        for method_index in range(len(self.methods)):
            method = self.methods[method_index]
            if method.name == search_term:
                init_index = method_index
                break
        
        if init_index < 0:
            return
        
        init_method = self.methods.pop(init_index)
        self.methods = [init_method] + self.methods
    
    
    def sort_methods(self):
        self.methods.sort()
    
    
    def sort_superclasses(self):
        self.superclasses.sort()
    
    
    def sort_attributes(self):
        self.attributes.sort()
    
    
    def update_attribute(self, attribute_doc):
        """Adds _AttributeDoc object uniquely regarding name.
        
        If no attribute with an equal name is present in the ".attributes"
        list, a new element will be added. Otherwise, the old attribute
        object with the same name will be overriden taking the old
        docstring if there is no docstring in the new object.
        
        Note reagrding the processing: The script goes through the source
        code from top to down. Actually, the case where there already is
        an attribute defined in the class body with the same name should
        never happen as the former ones defined will be overriden. However,
        syntactically this is still OK. Therefore, the right way to go is to
        stick to the latest attribute defined as it is the only one which
        will have any effect.
        """
        attribute_found = self.get_attribute(attribute_doc.name)
        
        if attribute_found:
            # "attribute_found" is actually a tuple.
            old_attribute = attribute_found[0]
            index         = attribute_found[1]
            
            if not attribute_doc.docstring:
                attribute_doc.docstring = old_attribute.docstring
            
            self.attributes[index] = attribute_doc
        else:
            self.attributes.append(attribute_doc)
    
    
    def update_method(self, new_method_doc):
        """Adds _FunctionDoc object uniquely regarding name.
        
        If no method with an equal name is present in the ".methods"
        list, a new element will be added. Otherwise, the old method
        object with the same name will be overriden taking the old
        docstring if there is no docstring in the new object.
        """
        method_found = None
        method_index = None
        
        for (index, method) in enumerate(self.methods):
            if method.name == new_method_doc.name:
                method_found = method
                method_index = index
                break
        
        if method_found:
            if not new_method_doc.docstring:
                new_method_doc.docstring = method_found.docstring
            
            self.methods[method_index] = new_method_doc
        else:
            self.methods.append(new_method_doc)



class _FunctionDoc(_DocBaseClass):
    """Represents doc of a top level function or method in a python module."""
    def __init__(self, ast_arg = None):
        self.docstring = None
        self.args      = None
    
    def args_string(self):
        """List of arguments as string."""
        string = ''
        
        for arg_doc_index in range(len(self.args)):
            arg_doc = self.args[arg_doc_index]
            string  += str(arg_doc)
            
            if arg_doc_index < (len(self.args) - 1):
                string += ', '
        
        return string



class _GlobalVarDoc(_DocBaseClass):
    """Represents doc of a global variable in a python moudle."""
    def __init__(self):
        self.docstring = None
        self.value     = None
    
    def __str__(self):
        string = f'{self.name}'
        
        if self.value:
            string += f' = {self.value}'
        
        return string



class _ModuleDoc(_DocBaseClass):
    """Represents top level view of abstract doc tree.
    
    As the only things we are interested in are classes, functions and
    global variables, these are the only things which are stored, here.
    """
    def __init__(self):
        self.classes     = list()
        self.docstring   = None
        self.functions   = list()
        self.global_vars = list()
    
    def sort(self):
        self.sort_classes()
        self.sort_functions()
        self.sort_global_vars()
    
    def sort_classes(self):
        self.classes.sort()
    
    def sort_functions(self):
        self.functions.sort()
    
    def sort_global_vars(self):
        self.global_vars.sort()



def _ast_args_to_arg_docs(ast_args, source):
    """Convert a ast.arguments object to list of _ArgDoc objects.
    
    Args:
        ast_args:
            type:
                ast.arguments
        source:
            type:
                str
            description:
                The source code string of the file where this "ast.arguments"
                object is from. Note that this is necessary to retrieve
                the default values from an argument list as there is no
                other convenient way to do so.
    """
    args              = ast_args.args
    defaults          = ast_args.defaults
    varargs           = ast_args.vararg  # variable argument list; e.g. *args
    keyword_args      = ast_args.kwarg  # keyword argument dict; e.g. **kwargs
    keyword_only_args = ast_args.kwonlyargs  # e.g. (*args, kw_only_arg = 1)
    keyword_defaults  = ast_args.kw_defaults
    args_list         = list()
    
    # Process in order in which args appear in arg list.
    for arg_index in range(len(args)):
        arg          = args[arg_index]
        arg_doc      = _ArgDoc()
        arg_doc.name = arg.arg
        
        # The "defaults" list is a bit tricky as it only contains
        # so many entries as there are actually defaults specified.
        # Note that only the last arguments can have default values,
        # only, as it is syntax error otherwise. A neat trick here is
        # used by calculating the theoratical default index position and
        # if this index is beyond the defaults list it is known that this
        # argument does not have a default.
        default_index = arg_index - (len(args) - len(defaults))
        
        if default_index >= 0:
            default         = defaults[default_index]
            line            = default.lineno
            column          = default.col_offset
            arg_doc.default = _extract_default(source, line, column)
        
        args_list.append(arg_doc)
    
    # variable arguments list
    if varargs:
        arg_doc            = _ArgDoc()
        arg_doc.name       = varargs.arg
        arg_doc.is_varargs = True
        args_list.append(arg_doc)
    
    # keyword only args
    for arg_index in range(len(keyword_only_args)):
        arg           = keyword_only_args[arg_index]
        arg_doc       = _ArgDoc()
        arg_doc.name  = arg.arg
        default_index = \
            arg_index - (len(keyword_only_args) - len(keyword_defaults))
        
        if default_index >= 0:
            default         = keyword_defaults[default_index]
            if default:
                line            = default.lineno
                column          = default.col_offset
                arg_doc.default = _extract_default(source, line, column)
        
        args_list.append(arg_doc)
    
    # keyword arguments dict
    if keyword_args:
        arg_doc           = _ArgDoc()
        arg_doc.name      = keyword_args.arg
        arg_doc.is_kwargs = True
        args_list.append(arg_doc)
    
    return args_list



def _extract_default(source, line_begin, column_begin):
    """Extract an argument default value from a source string.
    
    Note that the provided position into the source string "source"
    is assumed to really mark the beginning of some argument default
    value. If this is a random position into the source string, the output
    of this function may be an arbitrary part of the source beginning from
    the given position or an exception will be raised. However, this should
    not be the case as long as the abstract syntax tree is used to get line
    and column numbers.
    
    Note that this function is pretty much useless for python version >=3.8
    as the "ast" module already marks the end of the source in each AST node.
    However, as the target python version is 3.6 and for some very dumb
    reason nobody thought about marking the end in the source beforehand even
    though this information is easily available during build up of abstract
    syntax tree, it is needed to stick to some own solution, here.
    
    Returns:
        type:
            str
    
    Args:
        source:
            type:
                str
            description:
                Source code string where the default argument value resides.
        line_begin:
            type:
                int
            data:
                >=0
            description:
                The line index (starting from 1) into the source string where
                the argument default value begins.
        column_begin:
            type:
                int
            data:
                >=0
            description:
                The column index (starting from 0) into the source string
                where the argument default value begins.
        last_arg:
            type:
                bool
            description:
                "True" if this default belongs to the last argument in the
                argument list.
    """
    
    # Following variables are "current pointer" into the source string.
    line         = line_begin - 1  # due to "1-indexing" of argument
    column       = column_begin
    source_lines = source.splitlines()
    
    # Go through character by character. You know that you have reached
    # the end of a default if you encounter a comma while the braces
    # counter is zero (imagine "a = 355, b ..." where you now that
    # the default value 355 ends with the comma) or if the braces counter
    # reaches a negative value (imagine "func(a, b = 355)" where the end
    # of the default value is with the closing round brace). Note that
    # the counting of round braces is necessary as there might be
    # arbitrarily many opening and closing round braces (imagine
    # "(a, b = func1(Class(22)))".
    # By going through the string, get rid of multiple white spaces,
    # and any tabs in order to clean this part of the code. Note that
    # due to prior splitlines, newlines are already removed.
    braces_count        = 0
    white_space_counter = 0
    characters          = list()
    
    while True:
        if line >= len(source_lines):
            raise ValueError(f'Attempt to acces line {line+1} but the '
                             f'file only has {len(source_lines)} lines.')
        
        line_string = source_lines[line]
        
        if column >= len(line_string):
            raise ValueError(f'Line {line+1}: Attempt to access column '
                             f'{column+1} but this line only has '
                             f'{len(line_string)} columns.')
        
        character = line_string[column]
        
        if character == '\t':
            white_space_counter = 0
        
        elif character == ' ':
            white_space_counter += 1
            if white_space_counter <= 1:
                characters.append(character)
        
        elif character == ')':
            white_space_counter = 0
            braces_count        -= 1
            if braces_count < 0:
                break
            else:
                characters.append(character)
        
        elif character == '(':
            white_space_counter = 0
            braces_count        += 1
            characters.append(character)
        
        elif character == ',':
            white_space_counter = 0
            if braces_count == 0:
                break
            else:
                characters.append(character)
        else:
            white_space_counter = 0
            characters.append(character)
        
        column += 1
        if column >= len(line_string):
            line   += 1
            column = 0
            if line >= len(source_lines):
                raise ValueError('End of line reached but end of default '
                                 'value is still not detected.')
    
    # Clean succeeding "white space" like stuff if present.
    string = ''.join(characters)
    if len(string) > 0:
        if string[-1] == ' ':
            string = string[0 : -1]
    
    return string



def _extract_value(assign_obj, source):
    """Extracts the value from the source code using an ast.Assign object.
    
    The source is directly extracted from source rather than the ast.Assign
    object. The object is used to retrieve position, only. Getting from the
    source itself is actually the best way (as of 3.6) to deal with the value
    as the value may contain functions and methods, too. Treating all possible
    subexpressions would lead to much more effort after all.
    
    Returns:
        type:
            str or NoneType
        description:
            "None" if extraction of value wasn't successful. The string
            containing value otherwise.
    
    Args:
        assign_obj:
            type:
                ast.Assign
            description:
                Object which will be used to retrieve position information.
        source:
            type:
                str
            description:
                The exact source code string which is currently processed
                by this script.
    """
    ast_value    = assign_obj.value
    begin_line   = ast_value.lineno - 1
    begin_column = ast_value.col_offset
    source_lines = source.splitlines()
    
    if begin_column == -1:
        # This should normally not happen. However, semanticly this
        # means "end of previous line" which actually means the same as
        # "begin of current line" as the last character from the
        # previous line should not be included.
        begin_column = 0
    
    
    # Search for end line and end column.
    
    # Note: As of python 3.6, this is considered the best solution. A
    # seemingly better approach could be to look out for the next AST
    # node and get it's begin line and column and consider it the end
    # line and column of this AST Assign node. However, this is not only
    # problematic because of some position bugs in 3.6 AST but also due
    # to the fact that there is not always a following AST node on the
    # same hierarchy level. This is the case when a global variable is
    # at the very end of a file or if a class attribute in the __init__
    # method is assigned at the very end of the method. In the last
    # case, you cannot get around this manual search anyways.
    
    # The value of this assignment is over when an end of file or new
    # line without a line break indicator "\" is encountered. New lines
    # are also part of this value if it is inside a pair of colons.
    current_line_index = begin_line
    current_column     = begin_column
    
    # Note: Due to some bug in 3.6, it may be the case that an opening
    # brace is not at the specified begin column but rather one place
    # before that. If this is the case, correct the begin column.
    if current_column > 0:
        source_begin_line  = source_lines[current_line_index]
        previous_character = source_begin_line[current_column - 1]
        
        if previous_character in {'(', '[', '{'}:
            current_column -= 1
    
    
    # determine if this is triple quote string
    is_triple_quote_string = False
    
    if (current_column + 2) < len(source_lines[current_line_index]):
        current_line      = source_lines[current_line_index]
        first_three_chars = current_line[current_column: current_column + 3]
        
        if first_three_chars == '"""':
            is_triple_quote_string = True
            current_column         += 3
            
            if current_column >= len(current_line):
                current_line_index += 1
                current_column     = 0
                current_line       = source_lines[current_line_index]
    
    if is_triple_quote_string:
        # Following implementation seems to be OK. However, in 3.6,
        # there is this very nasty bug where the position ("begin_line",
        # "begin_column") is somehow at the """ at the very end of the
        # string rather than at the beginning. Therefore, simply ignore
        # such string values as long as this bug exists. Triple quote
        # strings are very likely to be so long that including them into
        # the docs would probably not be good anyways.
        # Note: The following code actually supports capturing of triple
        # quote strings if the position data would be correct.
        return None
    
    
    # Parse source string character by character. Collect all desired
    # characters in a list and combine the list to a string, later.
    # While collecting the characters, make sure to collapse multiple
    # non printing characters which are following each other into a
    # single white space. Also, leave out comments and do not include
    # the newlines as part of the value.
    value_chars          = list()
    current_line         = source_lines[current_line_index]
    non_printing_counter = 0
    non_printing_chars   = {' ', '\t', '\a', '\b', '\f', '\r', '\v'}
    round_brace_counter  = 0
    square_brace_counter = 0
    curly_brace_counter  = 0
    double_quote_counter = 0
    
    while True:
        char                 = current_line[current_column]
        line_continue        = False
        new_line             = False
        is_double_quote      = False
        is_non_printing_char = False
        
        if char == '\n':
            # Note that a newline character should actually not occur as
            # the splitlines method removes newlines. Keep this here,
            # still; just in case.
            if (round_brace_counter < 1) and (square_brace_counter < 1) and \
                (curly_brace_counter < 1) and not is_triple_quote_string:
                break
            
            is_non_printing_char = True
            new_line             = True
        
        elif char == '#':
            if is_triple_quote_string:
                value_chars.append(char)
            else:
                # Treat as new line as there is no need to go up to the
                # new line character in case of a comment. Note,
                # however, that a newline character should actually not
                # occur as the splitlines method removes newlines. Keep
                # this here, still; just in case.
                if (round_brace_counter < 1) and (square_brace_counter < 1) \
                    and (curly_brace_counter < 1):
                    break
                
                is_non_printing_char = True
                new_line             = True
        
        elif char == '\\':
            if is_triple_quote_string:
                value_chars.append(char)
            else:
                # This is line continuation character. Continue with
                # following line.
                is_non_printing_char = True
                line_continue        = True
        
        elif char in non_printing_chars:
            is_non_printing_char = True
        
        elif char == '(':
            value_chars.append(char)
            round_brace_counter += 1
        
        elif char == ')':
            value_chars.append(char)
            round_brace_counter -= 1
            
            if round_brace_counter < 0:
                return None
        
        elif char == '[':
            value_chars.append(char)
            square_brace_counter += 1
        
        elif char == ']':
            value_chars.append(char)
            square_brace_counter -= 1
            
            if square_brace_counter < 0:
                return None
        
        elif char == '{':
            value_chars.append(char)
            curly_brace_counter += 1
        
        elif char == '}':
            value_chars.append(char)
            curly_brace_counter -= 1
            
            if curly_brace_counter < 0:
                return None
        
        elif char == '"':
            value_chars.append(char)
            is_double_quote = True
        
        else:
            value_chars.append(char)
        
        
        if is_non_printing_char:
            non_printing_counter += 1
            
            if non_printing_counter == 1:
                value_chars.append(' ')
        else:
            non_printing_counter = 0
        
        
        if is_triple_quote_string:
            if is_double_quote:
                double_quote_counter += 1
                
                if double_quote_counter >= 3:
                    break
            else:
                double_quote_counter = 0
        
        
        current_column += 1
        line_ended     = (current_column >= len(current_line))
        
        if line_ended or new_line or line_continue:
            if ((line_ended or new_line) and (round_brace_counter < 1)
                     and (square_brace_counter < 1) and
                     (curly_brace_counter < 1) and not is_triple_quote_string):
                break
            
            
            # Go to next non empty line.
            current_line = ''
            end_of_file  = False
            
            while (len(current_line) == 0):
                current_line_index += 1
                
                if current_line_index >= len(source_lines):
                    end_of_file = True
                    break
                
                current_line = source_lines[current_line_index]
            
            if end_of_file:
                break
            else:
                current_column = 0
    
    if len(value_chars) > 0:
        last_char = value_chars[-1]
        
        if last_char == ' ':
            value_chars.pop()
    
    value_string = ''.join(value_chars)
    return value_string



def _ignore_name(name):
    """Checks whether given name shall be ignored according to options."""
    if name in IGNORE_NAMES:
        return True
    
    for ignore_prefix in IGNORE_NAME_PREFIXES:
        if len(ignore_prefix) > len(name):
            continue
        
        prefix = name[0 : len(ignore_prefix)]
        
        if prefix == ignore_prefix:
            return True
    
    return False



def _ast_class_to_doc(ast_class, source):
    """Convert ast.Class object to _ClassDoc object."""
    class_doc       = _ClassDoc()
    class_doc.name  = ast_class.name
    class_docstring = ast.get_docstring(ast_class)
    
    if DOCSTRING_PARSER_ENABLED:
        class_docstring = _parse_docstring(class_docstring)
    
    class_doc.docstring = class_docstring
    
    if IGNORE_WITHOUT_DOCSTRING and not class_doc.docstring:
        return None
    
    # From which classes does this class inherit?
    for superclass in ast_class.bases:
        class_doc.superclasses.append(superclass.id)
    
    if SORT_INHERITANCE_LIST:
        class_doc.sort_superclasses()
    
    leaf_nodes = ast_class.body
    
    for leaf_node_index in range(len(leaf_nodes)):
        leaf_node = leaf_nodes[leaf_node_index]
        
        if type(leaf_node) not in {ast.FunctionDef, ast.Assign}:
            continue
        
        
        if (type(leaf_node) == ast.Assign) and not IGNORE_ATTRIBUTES:
            attribute_doc                = _AttributeDoc()
            attribute_doc.class_variable = True
            targets                      = leaf_node.targets
            
            # Assignments might have multiple targets, e.g. by assigning
            # to list or tuple. Ignore such stuff according to PEP 258.
            # Note that this should really not be possible in a class
            # body but it is still good to have this code as a safety
            # measure.
            if len(targets) > 1:
                continue
            
            
            # Check if the supposed attribute (left hand side of the
            # assignment) has the right type and skip if not. This is necessary
            # as it may also be something different (don't get what, though,
            # but there have been definitely some bugs observed beforehand).
            # Also, just as a precaution, check if the object has the
            # attributes "id".
            ast_attribute = targets[0]
            
            if type(ast_attribute) not in {ast.Attribute, ast.Name}:
                continue
            
            if not hasattr(ast_attribute, 'id'):
                continue
            
            
            attribute_doc.name = ast_attribute.id
            
            if _ignore_name(attribute_doc.name):
                continue
            
            # get docstring if available
            if (leaf_node_index + 1) < len(leaf_nodes):
                next_leaf_node = leaf_nodes[leaf_node_index + 1]
            else:
                next_leaf_node = None
            
            if type(next_leaf_node) == ast.Expr:
                value = next_leaf_node.value
                
                # 3.7 and earlier: ast.Str
                # 3.8 and later: ast.Constant
                if type(value) in {ast.Str, ast.Constant}:
                    attribute_doc.docstring = cleandoc(value.s)
            
            attribute_doc.default = _extract_value(leaf_node, source)
            class_doc.update_attribute(attribute_doc)
        
        
        elif type(leaf_node) == ast.FunctionDef:
            method_name = leaf_node.name
            
            if _ignore_name(method_name) and not (method_name == '__init__'):
                continue
            
            # If the method has a decorator which ends with ".setter",
            # ignore it as it is part of an attribute definition. The
            # docstring of this attribute will be taken from the method
            # which is the getter. Therefore, also see whether this
            # method actually has the decorator "property".
            is_property = False
            is_setter   = False
            decorators  = leaf_node.decorator_list
            
            for decorator in decorators:
                if hasattr(decorator, 'attr'):
                    if decorator.attr == 'setter':
                        is_setter = True
                        break
                if decorator.id == 'property':
                    is_property = True
            
            if is_setter:
                continue
            
            if is_property:
                # Properties are considered attributes.
                attribute_name = leaf_node.name
            
            if is_property and (IGNORE_PROPERTIES or IGNORE_ATTRIBUTES):
                # Before going to the next class body node, mark
                # a potentially existing attribute with equal name
                # as an instance variable because this property is
                # basically overriding any class variable.
                equal_name_attr = class_doc.get_attribute(attribute_name)
                
                if equal_name_attr:
                    equal_name_attr = equal_name_attr[0]
                    equal_name_attr.class_variable = False
                
                continue
            
            docstring = ast.get_docstring(leaf_node)
            
            if is_property:
                # However, only add such attributes if the property
                # also has a docstring.
                if not docstring:
                    # Before going to the next class body node, mark a
                    # potentially existing attribute with equal name as an
                    # instance variable because this property is basically
                    # overriding any class variable.
                    equal_name_attr = class_doc.get_attribute(attribute_name)
                    
                    if equal_name_attr:
                        equal_name_attr = equal_name_attr[0]
                        equal_name_attr.class_variable = False
                    
                    continue
                
                if _ignore_name(leaf_node.name):
                    continue
                
                attribute_doc           = _AttributeDoc()
                attribute_doc.name      = attribute_name
                attribute_doc.docstring = docstring
                
                class_doc.update_attribute(attribute_doc)
                continue
            
            # look out for futher attributes in __init__ method
            if (method_name == '__init__') and not IGNORE_ATTRIBUTES:
                init_method = leaf_node.body
                
                for init_node_index in range(len(init_method)):
                    init_node = init_method[init_node_index]
                    
                    if not type(init_node) == ast.Assign:
                        continue
                    
                    if len(init_node.targets) > 1:
                        continue
                    
                    target = init_node.targets[0]
                    
                    if type(target) is not ast.Attribute:
                        # E.g. if we have a simple assignment to some variable
                        # in the method, we would get an "ast.Name".
                        continue
                    
                    attribute = target
                    
                    if attribute.value.id != 'self':
                        continue
                    
                    attribute_name = attribute.attr
                    
                    if _ignore_name(attribute_name):
                        continue
                    
                    if (init_node_index + 1) < len(init_method):
                        next_init_node = init_method[init_node_index + 1]
                    else:
                        next_init_node = None
                    
                    attr_docstring = None
                    
                    if type(next_init_node) == ast.Expr:
                        value = next_init_node.value
                        
                        # 3.7 and earlier: ast.Str
                        # 3.8 and later: ast.Constant
                        if type(value) in {ast.Str, ast.Constant}:
                            attr_docstring = cleandoc(value.s)
                    
                    
                    # Attributes in the class body (class variables) and
                    # properties have precedence. However, it might be
                    # the case that a corresponding class variable does
                    # not come with a docstring. In such a case, take at
                    # least the docstring from here.
                    found_attribute = class_doc.get_attribute(attribute_name)
                    
                    if found_attribute:
                        # "found_attribute" is actually a tuple which
                        # also contains the index.
                        found_attribute = found_attribute[0]
                        
                        if not found_attribute.docstring:
                            found_attribute.docstring = attr_docstring
                        continue
                    
                    attribute_doc           = _AttributeDoc()
                    attribute_doc.name      = attribute_name
                    attribute_doc.docstring = attr_docstring
                    attribute_doc.default   = _extract_value(init_node, source)
                    class_doc.attributes.append(attribute_doc)
            
            
            # Note: It is actually important to do this check so late in
            # order to be able to extract attributes from "__init__"
            # method if this is one.
            method_docstring = ast.get_docstring(leaf_node)
            
            if DOCSTRING_PARSER_ENABLED:
                method_docstring = _parse_docstring(method_docstring)
            
            if IGNORE_WITHOUT_DOCSTRING and not method_docstring:
                continue
            
            
            if REPLACE_INIT_METHOD_NAME and (method_name == '__init__'):
                method_name = class_doc.name
            
            method_doc           = _FunctionDoc()
            method_doc.name      = method_name
            method_doc.docstring = method_docstring
            args                 = leaf_node.args
            method_doc.args      = _ast_args_to_arg_docs(args, source)
            class_doc.update_method(method_doc)
    
    # Remove attributes which do not come with a docstring if necessary.
    # Note: This is done so late as there might be different sources of
    # attributes where one comes with a docstring and the other does
    # not. Therefore, this check is best done, here, where all
    # attributes are already collected.
    if IGNORE_WITHOUT_DOCSTRING:
        new_attributes = list()
        
        for attribute in class_doc.attributes:
            if attribute.docstring:
                new_attributes.append(attribute)
        
        class_doc.attributes = new_attributes
    
    # Parse docstring of attributes if necessary.
    # Note: This is done so late since the final attribute docstrings are only
    # known at this point.
    if DOCSTRING_PARSER_ENABLED:
        for attribute in class_doc.attributes:
            attribute.docstring = _parse_docstring(attribute.docstring)
    
    if SORT_METHODS:
        class_doc.sort_methods()
    
    if SORT_ATTRIBUTES:
        class_doc.sort_attributes()
    
    class_doc.put_init_first()
    return class_doc



def _ast_function_to_doc(ast_function, source):
    """convert ast.FunctionDef to _FunctionDoc object"""
    function_doc = _FunctionDoc()
    docstring    = ast.get_docstring(ast_function)
    
    if DOCSTRING_PARSER_ENABLED:
        docstring = _parse_docstring(docstring)
    
    function_doc.docstring = docstring
    
    if IGNORE_WITHOUT_DOCSTRING and not function_doc.docstring:
        return None
    
    function_doc.name = ast_function.name
    
    if _ignore_name(function_doc.name):
        return None
    
    args              = ast_function.args
    function_doc.args = _ast_args_to_arg_docs(args, source)
    return function_doc



def _ast_global_var_to_doc(ast_global_var, source, next_ast_obj):
    """convert ast.Assign object to _GlobalVarDoc
    
    It is assumed that the ast.Assign object is as global variable.
    """
    
    # Note that you can have multiple targets in an assignment; e.g. to
    # tuple or list. Ignore such stuff in accordance to PEP 258.
    targets = ast_global_var.targets
    if len(targets) > 1:
        return None
    
    global_var_doc = _GlobalVarDoc()
    global_var_doc.name = targets[0].id
    
    if _ignore_name(global_var_doc.name):
        return None
    
    # Look into the very next ast object and see whether it is an
    # expression including a string, only. If such as following string
    # exists, it is considered the docstring of this global variable
    # according to PEP 258.
    if next_ast_obj and (type(next_ast_obj) == ast.Expr):
        value = next_ast_obj.value
        
        # 3.7 and earlier: ast.Str
        # 3.8 and later: ast.Constant
        if type(value) in {ast.Str, ast.Constant}:
            docstring = cleandoc(value.s)
            
            if DOCSTRING_PARSER_ENABLED:
                docstring = _parse_docstring(docstring)
            
            global_var_doc.docstring = docstring
    
    if IGNORE_WITHOUT_DOCSTRING and not global_var_doc.docstring:
        return None
    
    # get value string
    if not global_var_doc.name in IGNORE_VALUES:
        value = _extract_value(ast_global_var, source)
        global_var_doc.value = value
    
    return global_var_doc



def _abstract_doc_tree(source, module_name):
    """Converts source string into an intermediate abstract doc tree.
    
    Returns:
        type:
            _ModuleDoc
        description:
            Abstract doc tree which is accessible through this top level
            object.
    
    Args:
        source:
            type:
                str
            description:
                Code source string.
        module_name:
            type:
                str
            description:
                Name of the module. This is needed as the source code itself
                does not contain the name of the module itself.
    """
    module_ast = ast.parse(source)
    
    if type(module_ast) != ast.Module:
        raise TypeError(f'Top level syntax tree node of the source code is '
                        f'expected to be of type "ast.Module" but is '
                        f'{type(module_ast)}.')
    
    module_doc           = _ModuleDoc()
    module_doc.docstring = ast.get_docstring(module_ast)
    module_doc.name      = module_name
    sub_nodes            = module_ast.body
    
    for sub_node_index in range(len(sub_nodes)):
        sub_node = sub_nodes[sub_node_index]
        
        if (sub_node_index + 1) < len(sub_nodes):
            next_ast_obj = sub_nodes[sub_node_index + 1]
        else:
            next_ast_obj = None
        
        
        if type(sub_node) == ast.FunctionDef:
            if _ignore_name(sub_node.name):
                continue
            
            function_doc = _ast_function_to_doc(sub_node, source)
            
            if not function_doc:
                continue
            
            # If there already was a function with the same name, take
            # the last which was defined since it is the only definition
            # which ahs any effect. However, if the latest function does
            # not come with a docstring but a previous definition came
            # with one, take the docstring of one of the previous
            # definitions.
            old_function_doc   = None
            old_function_index = None
            
            for (index, prev_function) in enumerate(module_doc.functions):
                if prev_function.name == function_doc.name:
                    old_function_doc   = prev_function
                    old_function_index = index
                    break
            
            if old_function_doc:
                if not function_doc.docstring:
                    function_doc.docstring = old_function_doc.docstring
                
                module_doc.functions[old_function_index] = function_doc
            else:
                module_doc.functions.append(function_doc)
        
        
        elif type(sub_node) == ast.Assign:
            if IGNORE_GLOBAL_VARS:
                continue
            
            global_var_doc = \
                _ast_global_var_to_doc(sub_node, source, next_ast_obj)
            
            if global_var_doc:
                module_doc.global_vars.append(global_var_doc)
        
        
        elif type(sub_node) == ast.ClassDef:
            if _ignore_name(sub_node.name):
                continue
            
            class_doc = _ast_class_to_doc(sub_node, source)
            
            if class_doc:
                module_doc.classes.append(class_doc)
    
    if SORT_CLASSES:
        module_doc.sort_classes()
    
    if SORT_FUNCTIONS:
        module_doc.sort_functions()
    
    if SORT_GLOBAL_VARS:
        module_doc.sort_global_vars()
    
    if DOCSTRING_PARSER_ENABLED:
        # replace raw docstring with parsed docstring
        docstring            = module_doc.docstring
        parsed_docstring     = _parse_docstring(docstring)
        module_doc.docstring = parsed_docstring
    
    return module_doc



#############
# DOC INDEX #
#############

# Note that this is not really a "doc" object but as the doc base class
# provides the ability for sorting by name, only, this is used here for
# the moment, anyways.
class _IndexModule(_DocBaseClass):
    """Represents the module data which is needed for the index."""
    def __init__(self, module_doc, path_list):
        """Args:
            module_doc:
                type:
                    _ModuleDoc
                description:
                    The _ModuleDoc object which shall be converted into
                    this index object.
            path_list:
                type:
                    list
                description:
                    The path to the module where the path is given by
                    a list of folders from base folder up to the module.
                    This must contain strings, only.
                    E.g.: 'base' -> 'subpackage' corresponds to
                    './base/subpackage'
        """
        self.name              = module_doc.name
        self.short_description = None
        
        # The first entry of the path list is the base folder name
        # which is not needed for the index. Replace it with a single
        # dot which indicates the current folder (which is the base
        # folder if in the base folder which is the case for the index).
        path_list[0]      = '.'
        self.package_path = '/'.join(path_list)
        
        if module_doc.docstring:
            docstring = module_doc.docstring
            
            # Short description is the first line of the module docstring
            # according to PEP 257.
            if type(docstring) is str:
                docstring_lines = docstring.splitlines()
                
                if len(docstring_lines) > 0:
                    self.short_description = docstring_lines[0]
            elif type(docstring) is _Docstring:
                # The _Docstring objects are kind of missing the raw docstring.
                # Therefore, it is not possible to determine the first line of
                # the module docstring according to the original docstring.
                # We only have the first paragraph. As a "good guess" we use
                # the first 76 characters of the first paragraph as the short
                # description assuming that the most common line limit is 79
                # characters and excluding the """ at the beginning of the
                # docstring. Note that by doing this we actually violate
                # PEP 257. A better approach would be to simply include the
                # cleaned docstring string as an additional attribute of the
                # _Docstring class.
                if len(docstring.paragraphs) < 1:
                    self.short_description = ''
                else:
                    first_paragraph = docstring.paragraphs[0]
                    
                    if len(first_paragraph) > 76:
                        self.short_description = first_paragraph[:76]
                    else:
                        self.short_description = first_paragraph



class _IndexIterator:
    def __init__(self, index):
        """Args:
            index:
                The _Index object over which to iteratore.
        """
        self.modules          = index._subpackages
        self.subpackage_names = index._subpackages.keys()
        self.subpackage_names = list(self.subpackage_names)
        self.subpackage_names.sort()
        self.current_index = 0
    
    def __next__(self):
        if self.current_index >= len(self.subpackage_names):
            raise StopIteration
        
        subpackage_name    = self.subpackage_names[self.current_index]
        subpackage_modules = self.modules[subpackage_name]
        self.current_index += 1
        
        return subpackage_modules, subpackage_name



class _Index:
    """Data structure for doc index.
    
    Add index entries to this index using method "add".
    Iterate over index entries using simple for loop.
    On for looping, the first element is the _IndexModule object
    and the second element is the subpackage name as a "python style
    path" (folder names seperated by dots without the base package).
    """
    def __init__(self, package_name):
        self.package_name = package_name
        
        # The package description will be taken from the docstring of the
        # __init__.py file. However, as the docs are generated as walking
        # through the files in the package, this is left out for the
        # moment. It should be added, later, if available.
        self.package_description = None
        
        # Used to indicate whether the modules in each subpackage list
        # is sorted. This improves performance a little bit as it is
        # only needed to sort all the module lists on read access, not
        # when adding modules.
        self._sorted = False
        
        # This dictionary contains the information of all modules which
        # is needed regarding the index. It contains lists, only. The
        # keys are the "python style paths" (folders seperated by dots)
        # to the subpackages (including the base package). Therefore,
        # each key contains all of the module index information for one
        # subpackage; thus, the entries are lists. The lists contain
        # _IndexModule objects, only. Use the method "add" instead of
        # directly accessing this list.
        self._subpackages = dict()
    
    def add(self, module_doc, module_path_list):
        """Add necessary module doc information into this index.
        
        Args:
            module_doc:
                type:
                    _ModuleDoc
                description:
                    The module doc from which to retrieve the data.
            module_path_list:
                type:
                    list
                description:
                    The path to the module where the path is given by
                    a list of folders from base folder up to the module.
                    This must contain strings, only.
                    E.g.: 'base' -> 'subpackage' corresponds to
                    './base/subpackage'
        """
        self._sorted = False
        module_index = _IndexModule(module_doc, module_path_list)
        
        # Remove the first entry from the path list as it is the name
        # of the base folder which shall not be included in the docs.
        # Note that the keys will also be used in the genmeration of the
        # docs themselves.
        module_path_list = module_path_list[1:]
        key              = '.'.join(module_path_list)
        
        if key not in self._subpackages:
            self._subpackages[key] = list()
        
        self._subpackages[key].append(module_index)
    
    def __iter__(self):
        if not self._sorted:
            for key in self._subpackages:
                self._subpackages[key].sort()
        
        return _IndexIterator(self)



##################
# HTML CONVERTER #
##################

def _docstring_to_HTML(docstring):
    """Convert a docstring into a "div" XmlTree element.
    
    Returns:
        type:
            XmlTree.Element
    """
    div = XmlTree.Element('div')
    
    # Docstring can be either "raw" (pure text input) or "parsed" ("_Docstring"
    # object).
    if type(docstring) is str:
        # Raw docstrings are displayed "as such"; every line in the docstring
        # is rendered as a seperate line of text in HTML.
        lines = docstring.splitlines()
        
        for line_index in range(len(lines)):
            line = lines[line_index]
            
            if len(line) == 0:
                XmlTree.SubElement(div, 'br')
            else:
                p      = XmlTree.SubElement(div, 'p')
                p.text = line
                p.set('class', 'line')
    elif type(docstring) is _Docstring:
        # Parsed docstrings are currently only disassembled into paragraphs
        # of text using the intelligent newline mechanism. These can be
        # directly translated into "p" HTML elements. Keep in mind that
        # blank lines are also considered own paragraphs, each. Therefore,
        # we put breaks for blank lines instead of "p" elements of course.
        # We deal with the indentation level by simply setting a corresponding
        # class name which encodes the level of indentation. This makes the
        # translated HTML rather clean and also slim but makes the CSS file
        # kind of ugly to be honest because we have to add dozens of
        # definitions in the CSS file for each level of indentation (keep in
        # mind that the indentation levels correspond to white spaces in the
        # original docstring so we need a lot of definitions). However, I did
        # not found a better solution. Maybe the indentation levels could be
        # made so that multiple white spaces are considered an indentation
        # level but this comes with more risk regarding the interpretation of
        # the docstrings and also adds a lot of code complexity.
        for paragraph_index in range(len(docstring.paragraphs)):
            paragraph = docstring.paragraphs[paragraph_index]
            
            if len(paragraph) == 0:
                XmlTree.SubElement(div, 'br')
            else:
                indentation = docstring.indentation_levels[paragraph_index]
                p           = XmlTree.SubElement(div, 'p')
                p.text      = paragraph
                class_name  = f'indent{indentation}'
                p.set('class', class_name)
    else:
        raise TypeError(f'Unknown type of docstring: {type(docstring)}')
    
    return div



def _index_HTML(index):
    """Args:
        index:
            type:
                _Index
            description:
                The index object which shall be converted to HTML string.
    """
    doctype_string = '<!DOCTYPE html>\n'
    root           = XmlTree.Element('html')
    head           = XmlTree.SubElement(root, 'head')
    title          = XmlTree.SubElement(head, 'title')
    title.text     = index.package_name
    
    stylesheet_path = './style.css'
    link            = XmlTree.SubElement(head, 'link')
    link.set('rel', 'stylesheet')
    link.set('type', 'text/css')
    link.set('href', stylesheet_path)
    
    body    = XmlTree.SubElement(root, 'body')
    h1      = XmlTree.SubElement(body, 'h1')
    h1.text = index.package_name
    
    if index.package_description:
        package_docstring = _docstring_to_HTML(index.package_description)
        package_docstring.set('class', 'module_docstring')
        body.append(package_docstring)
    
    if LINK_PROJECT:
        XmlTree.SubElement(body, 'br')
        a  = XmlTree.SubElement(body, 'a')
        
        if LINK_PROJECT_TEXT:
            a.text = LINK_PROJECT_TEXT
        else:
            a.text = LINK_PROJECT
        
        a.set('href', LINK_PROJECT)
        a.set('class', 'project_link')
    
    if LINK_REPO:
        XmlTree.SubElement(body, 'br')
        a = XmlTree.SubElement(body, 'a')
        
        if LINK_REPO_TEXT:
            a.text = LINK_REPO_TEXT
        else:
            a.text = LINK_REPO
        
        a.set('href', LINK_REPO)
        a.set('class', 'repo_link')
    
    h2      = XmlTree.SubElement(body, 'h2')
    h2.text = 'Index'
    h2.set('class', 'index')
    
    for modules, subpackage_name in index:
        subpackage_div = XmlTree.SubElement(body, 'div')
        subpackage_div.set('class', 'index_subpackage')
        
        if subpackage_name != '':
            subpackage_name_p = XmlTree.SubElement(subpackage_div, 'p')
            subpackage_name_p.set('class', 'index_subpackage_name')
            subpackage_name_p.text = f'subpackage {subpackage_name}:'
        
        for module in modules:
            index_module_div = XmlTree.SubElement(subpackage_div, 'div')
            index_module_div.set('class', 'index_module')
            
            relative_path = module.package_path.replace('\\', '/')
            relative_path = f'{module.package_path}/{module.name}.html'
            index_module_name_a = XmlTree.SubElement(index_module_div, 'a')
            index_module_name_a.text = module.name
            index_module_name_a.set('href', relative_path)
            index_module_name_a.set('class', 'index_module_name')
            
            if module.short_description and INDEX_SHORT_DESCRIPTION:
                description_p = XmlTree.SubElement(index_module_div, 'p')
                description_p.text = module.short_description
                description_p.set('class', 'index_module_description')
    
    
    html_string = XmlTree.tostring(root, encoding='unicode', method='html')
    html_string = doctype_string + html_string
    return html_string



def _HTML(module_doc, rel_base_path, module_name):
    """Module source into HTML documentation string.
    
    Furthermore, it extracts all information which could be useful for the
    generation of some index.
    
    Returns:
        type:
            str
        description:
            Ready to write HTML documentation string for the python module.
    
    Args:
        module_doc:
            type:
                _ModuleDoc
            description:
                The doc object of the module to be converted into HTML.
        rel_base_path:
            type:
                str
            description:
                Relative path from the current working directory to the base
                path which is the base of the python package. This is needed
                in order to be able to link to the index and CSS file
                properly.
        module_name:
            type:
                str
            description:
                Name of the module. This is needed as the source code itself
                does not contain the module name.
    """
    doctype_string = '<!DOCTYPE html>\n'
    root           = XmlTree.Element('html')
    head           = XmlTree.SubElement(root, 'head')
    title          = XmlTree.SubElement(head, 'title')
    title.text     = module_name
    
    rel_base_path   = rel_base_path.replace('\\', '/')
    stylesheet_path = join(rel_base_path, 'style.css')
    link            = XmlTree.SubElement(head, 'link')
    link.set('rel', 'stylesheet')
    link.set('type', 'text/css')
    link.set('href', stylesheet_path)
    
    body    = XmlTree.SubElement(root, 'body')
    h1      = XmlTree.SubElement(body, 'h1')
    h1.text = module_name
    
    if INDEX_SHOW_LINK:
        if rel_base_path[-1] != '/':
            rel_base_path += '/'
        
        index_path = rel_base_path + 'index.html'
        a = XmlTree.SubElement(body, 'a')
        a.set('href', index_path)
        a.set('class', 'index_link')
        
        if INDEX_LINK_TEXT:
            a.text = INDEX_LINK_TEXT
        else:
            a.text = index_path
    
    if module_doc.docstring:
        module_docstring = _docstring_to_HTML(module_doc.docstring)
        module_docstring.set('class', 'module_docstring')
        body.append(module_docstring)
    
    
    for Type in ORDER:
        if Type == 'global_vars':
            if len(module_doc.global_vars) == 0:
                continue
            
            global_vars_div = XmlTree.SubElement(body, 'div')
            global_vars_div.set('class', 'global_vars')
            h2      = XmlTree.SubElement(global_vars_div, 'h2')
            h2.text = HEADING_GLOBAL_VARS
            
            for global_var in module_doc.global_vars:
                global_var_div = XmlTree.SubElement(global_vars_div, 'div')
                global_var_div.set('class', 'global_var')
                p      = XmlTree.SubElement(global_var_div, 'p')
                p.text = str(global_var)
                
                if not global_var.docstring:
                    continue
                
                global_var_docstring = _docstring_to_HTML(global_var.docstring)
                global_var_docstring.set('class', 'global_var_docstring')
                global_var_div.append(global_var_docstring)
        
        
        elif Type == 'functions':
            if len(module_doc.functions) == 0:
                continue
            
            functions_div = XmlTree.SubElement(body, 'div')
            functions_div.set('class', 'functions')
            h2      = XmlTree.SubElement(functions_div, 'h2')
            h2.text = HEADING_FUNCTIONS
            
            for function in module_doc.functions:
                function_div = XmlTree.SubElement(functions_div, 'div')
                function_div.set('class', 'function')
                p      = XmlTree.SubElement(function_div, 'p')
                p.text = f'{function.name}({function.args_string()})'
                
                if not function.docstring:
                    continue
                
                function_docstring = _docstring_to_HTML(function.docstring)
                function_docstring.set('class', 'function_docstring')
                function_div.append(function_docstring)
        
        
        elif Type == 'classes':
            if len(module_doc.classes) == 0:
                continue
            
            classes_div = XmlTree.SubElement(body, 'div')
            classes_div.set('class', 'classes')
            h2      = XmlTree.SubElement(classes_div, 'h2')
            h2.text = HEADING_CLASSES
            
            for Class in module_doc.classes:
                class_div = XmlTree.SubElement(classes_div, 'div')
                class_div.set('class', 'class')
                
                h3      = XmlTree.SubElement(class_div, 'h3')
                h3.text = Class.name
                
                # If some inheritance list is there, show both, a heading
                # for inheritance list and a heading for the docstring.
                if len(Class.superclasses) > 0:
                    h4_1      = XmlTree.SubElement(class_div, 'h4')
                    h4_1.text = HEADING_CLASS_INHERITANCE_LIST
                    ul        = XmlTree.SubElement(class_div, 'ul')
                    ul.set('class', 'superclasses')
                    
                    for superclass in Class.superclasses:
                        li      = XmlTree.SubElement(ul, 'li')
                        li.text = superclass
                    
                    if Class.docstring:
                        h4_2      = XmlTree.SubElement(class_div, 'h4')
                        h4_2.text = HEADING_CLASS_DESCRIPTION
                
                if Class.docstring:
                    class_docstring = _docstring_to_HTML(Class.docstring)
                    class_docstring.set('class', 'class_docstring')
                    class_div.append(class_docstring)
                
                if len(Class.attributes) > 0:
                    attributes     = Class.attributes
                    h4_3           = XmlTree.SubElement(class_div, 'h4')
                    h4_3.text      = HEADING_ATTRIBUTES
                    attributes_div = XmlTree.SubElement(class_div, 'div')
                    attributes_div.set('class', 'attributes')
                    
                    for attribute in attributes:
                        attribute_div = XmlTree.SubElement(attributes_div,
                                                           'div')
                        attribute_div.set('class', 'attribute')
                        p      = XmlTree.SubElement(attribute_div, 'p')
                        p.text = str(attribute)
                        
                        if attribute.class_variable:
                            p      = XmlTree.SubElement(attribute_div, 'p')
                            p.text = CLASS_VARIABLE_TEXT
                            p.set('class', 'class_variable_hint')
                        
                        if attribute.docstring:
                            attribute_docstring = \
                                _docstring_to_HTML(attribute.docstring)
                            attribute_docstring.set('class',
                                                    'attribute_docstring')
                            attribute_div.append(attribute_docstring)
                
                h4_4      = XmlTree.SubElement(class_div, 'h4')
                h4_4.text = HEADING_METHODS
                
                for method in Class.methods:
                    method_div = XmlTree.SubElement(class_div, 'div')
                    method_div.set('class', 'method')
                    p      = XmlTree.SubElement(method_div, 'p')
                    p.text = f'{method.name}({method.args_string()})'
                    
                    if not method.docstring:
                        continue
                    
                    method_docstring = _docstring_to_HTML(method.docstring)
                    method_docstring.set('class', 'method_docstring')
                    method_div.append(method_docstring)
    
    
    html_string = XmlTree.tostring(root, encoding='unicode', method='html')
    html_string = doctype_string + html_string
    return html_string



####################
# HELPER FUNCTIONS #
####################

def _check_options():
    """Checks whether all options are there and as they are expected."""
    if type(CLASS_VARIABLE_TEXT) != str:
        raise TypeError('option "CLASS_VARIABLE_TEXT" is not a string')
    
    if type(DOC_PATH) != str:
        raise TypeError('option "DOC_PATH" is not a string')
    
    if type(DOCSTRING_PARSER_ENABLED) != bool:
        raise TypeError('option "DOCSTRING_PARSER_ENABLED" is not a bool')

    if type(DOCSTRING_PARSER_INTELLIGENT_NEWLINE) != bool:
        raise TypeError('option "DOCSTRING_PARSER_INTELLIGENT_NEWLINE" is '
                        'not a bool')
    
    if type(HEADING_ATTRIBUTES) != str:
        raise TypeError('option "HEADING_ATTRIBUTES" is not a string')
    
    if type(HEADING_CLASSES) != str:
        raise TypeError('option "" is not HEADING_CLASSES')
    
    if type(HEADING_CLASS_DESCRIPTION) != str:
        raise TypeError('option "HEADING_CLASS_DESCRIPTION" is not a string')
    
    if type(HEADING_CLASS_INHERITANCE_LIST) != str:
        raise TypeError('option "HEADING_CLASS_INHERITANCE_LIST" is not '
                        'a string')
    
    if type(HEADING_FUNCTIONS) != str:
        raise TypeError('option "HEADING_FUNCTIONS" is not a string')
    
    if type(HEADING_GLOBAL_VARS) != str:
        raise TypeError('option "HEADING_GLOBAL_VARS" is not a string')
    
    if type(HEADING_METHODS) != str:
        raise TypeError('option "HEADING_METHODS" is not string')
    
    if type(HTML_BASE_FONT_SIZE) not in {int, float}:
        raise TypeError('option "HTML_BASE_FONT_SIZE" is not a number.')
    
    if (HTML_BASE_FONT_SIZE <= 0) or (HTML_BASE_FONT_SIZE > 10):
        raise ValueError('option "HTML_BASE_FONT_SIZE" is not in range '
                         '0 ... 10')
    
    if type(INDEX_LINK_TEXT) not in {str, type(None)}:
        raise TypeError('option "INDEX_LINK_TEXT" is neither a string '
                        'nor "None"')
    
    if type(INDEX_SHORT_DESCRIPTION) != bool:
        raise TypeError('option "INDEX_SHORT_DESCRIPTION" is not a bool')
    
    if type(IGNORE_GLOBAL_VARS) != bool:
        raise TypeError('option "IGNORE_GLOBAL_VARS" is not a bool')
    
    if type(IGNORE_FILE_NAMES) != set:
        raise TypeError('option "IGNORE_FILE_NAMES" is not a set')
    
    for ignore_name in IGNORE_FILE_NAMES:
        if type(ignore_name) != str:
            raise TypeError('option "IGNORE_FILE_NAMES" contains non string '
                            'entries')
    
    if type(IGNORE_FOLDER_NAMES) != set:
        raise TypeError('option "IGNORE_FOLDER_NAMES" is not a set')
    
    for ignore_folder_name in IGNORE_FOLDER_NAMES:
        if type(ignore_folder_name) != str:
            raise TypeError('option "IGNORE_FOLDER_NAMES" contains non '
                            'string entries')
    
    if type(IGNORE_NAMES) != set:
        raise TypeError('option "IGNOIGNORE_NAMESRE_NAMES" is not a set')
    
    for ignoring_name in IGNORE_NAMES:
        if type(ignoring_name) != str:
            raise TypeError('option "IGNORE_NAMES" contains non string '
                            'entries')
    
    if type(IGNORE_NAME_PREFIXES) != set:
        raise TypeError('option "IGNORE_NAME_PREFIXES" is not a string')
    
    for ignoring_name_prefix in IGNORE_NAME_PREFIXES:
        if type(ignoring_name_prefix) != str:
            raise TypeError('option "IGNORE_NAME_PREFIXES" contains non '
                            'string entries')
    
    if type(IGNORE_VALUES) != set:
        raise TypeError('option "IGNORE_VALUES" is not a set')
    
    for ignoring_value in IGNORE_VALUES:
        if type(ignoring_value) != str:
            raise TypeError('option "IGNORE_VALUES" contains non string '
                            'entries')
    
    if type(IGNORE_PROPERTIES) is not bool:
        raise TypeError('option "IGNORE_PROPERTIES" is not a bool')
    
    if type(IGNORE_WITHOUT_DOCSTRING) != bool:
        raise TypeError('option "IGNORE_WITHOUT_DOCSTRING" is not a bool')
    
    if type(ORDER) not in {tuple, list}:
        raise TypeError('option "ORDER" is not a tuple')
    
    mandatory_order_items = {'global_vars', 'functions', 'classes'}
    
    for order_item in ORDER:
        if order_item not in mandatory_order_items:
            raise ValueError(f'option "ORDER" contains entries which are '
                             f'none of {mandatory_order_items}')
    
    if not mandatory_order_items.issubset(set(ORDER)):
        raise ValueError(f'option "ORDER" does not contain all of the items '
                         f'{mandatory_order_items}')
    
    if type(LINK_PROJECT) not in {str, type(None)}:
        raise TypeError('option "LINK_PROJECT" is neither a string nor "None"')
    
    if type(LINK_PROJECT_TEXT) != str:
        raise TypeError('option "LINK_PROJECT_TEXT" is not a string')
    
    if type(LINK_REPO) not in {str, type(None)}:
        raise TypeError('option "LINK_REPO" is neither a string nor "None"')
    
    if type(LINK_REPO_TEXT) != str:
        raise TypeError('option "LINK_REPO_TEXT" is not a string')
    
    if type(PACKAGE_NAME) not in {str, type(None)} :
        raise TypeError('option "PACKAGE_NAME" is neither a string nor "None"')
    
    if type(REPLACE_INIT_METHOD_NAME) != bool:
        raise TypeError('option "REPLACE_INIT_METHOD_NAME" is not a bool')
    
    if type(SORT_ATTRIBUTES) != bool:
        raise TypeError('option "SORT_ATTRIBUTES" is not a bool')
    
    if type(SORT_CLASSES) != bool:
        raise TypeError('option "SORT_CLASSES" is not a bool')
    
    if type(SORT_INHERITANCE_LIST ) != bool:
        raise TypeError('option "SORT_INHERITANCE_LIST " is not a bool')
    
    if type(SORT_FUNCTIONS) != bool:
        raise TypeError('option "SORT_FUNCTIONS" is not a bool')
    
    if type(SORT_GLOBAL_VARS) != bool:
        raise TypeError('option "SORT_GLOBAL_VARS" is not a bool')
    
    if type(SORT_METHODS) != bool:
        raise TypeError('option "SORT_METHODS" is not a bool')
    
    if type(SPACES_PER_TAB) != int:
        raise TypeError('option "SPACES_PER_TAB" is not an integer')
    
    if SPACES_PER_TAB < 1:
        raise ValueError('option "SPACES_PER_TAB" is smaller than 1')
    
    if SPACES_PER_TAB > 8:
        raise ValueError('Option "SPACES_PER_TAB" is greater than 8 which is '
                         'considered way too much. You would not get a '
                         'proper output with this setting.')
    
    if type(CSS) != str:
        raise TypeError('option "CSS" is not a string')



def _path_to_list(path, base_folder_name):
    """Converts the path given as string into a list.
    
    This basically removes the folder delimiters from the string.
    It searches backwards until it reaches the given "base_folder_name".
    Therefore, all folder names which are before the "base_folder_name"
    in the path string will not be part of the output path list.
    """
    path, current_folder_name = split(path)
    path_list = deque()
    path_list.appendleft(current_folder_name)
    
    while (path != '') and (current_folder_name != base_folder_name):
        path, current_folder_name = split(path)
        path_list.appendleft(current_folder_name)
    
    return list(path_list)



def _subfolder_list(base_folder_path):
    """Searches in base folder path for all subfolders.
    
    Returns:
        type:
            list
        elements:
            type:
                str
            description:
                Path to some folder or base folder. If the base folder path
                is a relative path, all paths are relative, too.
        description:
            For each subfolder found, there will be an entry in this
            list. The base folder itself (the folder in which the search
            will be done) is included, too.
    """
    previous_working_directory = getcwd()
    chdir(base_folder_path)
    
    # Build up the list of folder paths by starting off with the
    # current directory and getting all folders in this directory.
    # The same procedure is repeated for each new folder path found
    # this way until all folder paths are collected.
    folder_paths       = [base_folder_path]
    folder_index       = 0
    
    while folder_index < len(folder_paths):
        current_folder_path = folder_paths[folder_index]
        chdir(current_folder_path)
        
        for node_path in listdir():
            if isdir(node_path):
                # All paths stored are relative to this script.
                node_path_expanded = join(current_folder_path, node_path)
                folder_paths.append(node_path_expanded)
        
        folder_index += 1
        chdir(base_folder_path)
    
    chdir(previous_working_directory)
    return folder_paths



###############
# ENTRY POINT #
###############

def main(path = None):
    """Main function of this module.
    
    This function will be called if this module is executed as a script. This
    means that you can achieve exactly the same as the execution of this module
    as a script by simply calling this function. Note that by default the docs
    are generated for the folder where this script resides in. However, you can
    change this behaviour by passing the argument "path".
    
    Args:
        path:
            type:
                str
            description:
                The base path of the python code. The documentation will be
                generated for python code in this folder and all subfolders
                arbitrarily deep.
    """
    
    if version_info.major < 3:
        raise Exception(f'python {version_info.major} is not supported. '
                        f'Oldest version supported is 3.6. Ending.')
    
    elif version_info.minor < 6:
        raise Exception(f'python {version_info.major}.{version_info.minor} '
                        f'is not supported. Oldest version supported is 3.6. '
                        f'Ending.')
    
    _check_options()
    
    if not path:
        # "realpath" resolves symbolic links.
        code_path = dirname(normpath(realpath(__file__)))
    else:
        code_path = abspath(path)
    
    doc_path = abspath(DOC_PATH)
    
    if not isdir(doc_path):
        makedirs(doc_path)
    
    chdir(code_path)
    folder_paths     = _subfolder_list(code_path)
    base_folder_name = split(code_path)[1]
    
    if PACKAGE_NAME:
        package_name = PACKAGE_NAME
    else:
        package_name = base_folder_name
    
    init_doc     = None  # module doc of __init__.py in base folder
    index        = _Index(package_name)
    
    # Get all python source strings in all folders and generate the
    # HTML documentation for it in the corresponding doc folder.
    # Also, build up the needed index.
    for folder_index in range(len(folder_paths)):
        folder_path = folder_paths[folder_index]
        
        # Ignore all folders which reside inside the doc folder.
        # Comapre the beginning of all paths to do so.
        if len(folder_path) >= len(doc_path):
            folder_path_begin = folder_path[:len(doc_path)]
            
            if folder_path_begin == doc_path:
                continue
        
        
        # Ignore specified folders.
        relative_path = relpath(folder_path, code_path)  # from base
        path_list     = _path_to_list(folder_path, base_folder_name)
        ignore_folder = False
        
        for ignoring_folder_name in IGNORE_FOLDER_NAMES:
            if ignoring_folder_name in path_list:
                ignore_folder = True
                break
        
        if ignore_folder:
            continue
        
        
        relative_path = relpath(folder_path, code_path)  # from base
        rel_base_path = relpath(code_path, folder_path)  # to base
        chdir(folder_path)
        corresponding_doc_path = join(doc_path, relative_path)
        corresponding_doc_path = normpath(corresponding_doc_path)
        
        if not isdir(corresponding_doc_path):
            makedirs(corresponding_doc_path)
        
        for node_name in listdir():
            if not isfile(node_name):
                continue
            
            node_path          = join(folder_path, node_name)
            file_name          = split(node_name)[1]
            extension_splitted = splitext(file_name)
            base_file_name     = extension_splitted[0]
            extension          = extension_splitted[1]
            HTML_file_name     = base_file_name + '.html'
            is_init            = False
            
            if base_file_name in IGNORE_FILE_NAMES:
                continue
            
            # Ignore all __init__ modules besides the one in the
            # base folder which is always the very first folder.
            if file_name == '__init__.py':
                if folder_index == 0:
                    is_init = True
                else:
                    continue
            
            if extension == '.py':
                try:
                    module_file = open(file_name, 'r')
                except FileNotFoundError as error:
                    message = f'Error during opening of file "' \
                              f'{file_name}". Exception message: \n' \
                              f'{error}\nSkipping this file.'
                    print(message)
                    continue
                
                source = module_file.read()
                module_file.close()
                
                try:
                    module_doc = _abstract_doc_tree(source, base_file_name)
                except Exception as error:
                    message = (f'Something went wrong in parsing of file'
                               f' "{node_path}". Skipping this file. '
                               f'Error message:\n{error}\n')
                    print(message, flush = True)
                    traceback = format_exception()
                    print(traceback, flush = True)
                    continue
                
                if is_init:
                    init_doc = module_doc
                    continue
                
                try:
                    HTML_string = _HTML(module_doc, rel_base_path,
                                        base_file_name)
                except Exception as error:
                    message = (f'Something went wrong in HTML generation for '
                               f'file "{node_path}". Skipping this file. '
                               f'Error message:\n{error}\n')
                    print(message, flush = True)
                    traceback = format_exception()
                    print(traceback, flush = True)
                    continue
                
                chdir(corresponding_doc_path)
                
                try:
                    HTML_file = open(HTML_file_name, 'w')
                except Exception as error:
                    message = f'Error during opening of file "' \
                              f'{HTML_file_name}". Exception message:\n' \
                              f'{error}\nSkipping this file.'
                    print(message)
                    chdir(code_path)
                    continue
                
                HTML_file.write(HTML_string)
                HTML_file.close()
                chdir(folder_path)
                index.add(module_doc, path_list)
        
        chdir(code_path)
    
    
    # Generate and write out CSS file.
    global CSS
    CSS = CSS.replace('###HTML_BASE_FONT_SIZE###', str(HTML_BASE_FONT_SIZE))
    half_font_size = str(HTML_BASE_FONT_SIZE / 2)
    CSS = CSS.replace('###HALF_HTML_BASE_FONT_SIZE###', half_font_size)
    CSS_path = join(DOC_PATH, 'style.css')
    CSS_path = normpath(CSS_path)
    CSS_path = CSS_path.replace('\\', '/')
    CSS_file = open(CSS_path, 'w+')
    CSS_file.write(CSS)
    CSS_file.close()
    
    
    # generate index
    if init_doc and init_doc.docstring:
        index.package_description = init_doc.docstring
    
    index_HTML_string = _index_HTML(index)
    chdir(DOC_PATH)
    open_successful = True
    
    try:
        index_file = open('index.html', 'w')
    except Exception as error:
        path = join(corresponding_doc_path, 'index.html')
        open_successful = False
        print(f'Could not open file "{path}":\n{error}\n'
              f'Skipping the "index.html" file.')
    
    if open_successful:
        index_file.write(index_HTML_string)
        index_file.close()
    
    chdir(code_path)



if __name__ == '__main__':
    main()
